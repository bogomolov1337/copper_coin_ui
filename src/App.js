import React from 'react';
import { Provider } from 'react-redux';
import { store } from './store';
import ContextContainer from './components/userContext/ContextContainer';

const App = () => (
  <Provider store={store}>
    <ContextContainer />
  </Provider>
);

export default App;
