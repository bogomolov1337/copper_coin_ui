import React from 'react';
import ReactDOM from 'react-dom';
import i18next from 'i18next';
import { I18nextProvider } from 'react-i18next';
import * as serviceWorker from './serviceWorker';
import { resources } from './i18n';
import App from './App';

i18next.init({ resources, lng: 'en' });

ReactDOM.render(
  <I18nextProvider i18n={i18next}>
    <App />
  </I18nextProvider>,
  document.getElementById('root'),
);

serviceWorker.unregister();
