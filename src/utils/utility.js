import React from 'react';
import { defaultPageRequest, locale } from '../constants/settings';

export function getTxsRequestData(count, pagenumber, pagesize) {
  return { count, pageNumber: pagenumber, pageSize: pagesize };
}

export function getPageRequest(selectedCompanies) {
  const pageInfo = sessionStorage.getItem('txsPageReqInfo');
  const pageArg = (pageInfo) ? JSON.parse(pageInfo) : getTxsRequestData(selectedCompanies[0].name, 1, 1, 10);
  sessionStorage.setItem('txsPageReqInfo', JSON.stringify(pageArg));
  return pageArg;
}

export function getCompanyPageRequest(selectedCompany) {
  const pageInfo = sessionStorage.getItem('txsPageReqInfo');
  const pageArg = (pageInfo) ? JSON.parse(pageInfo)
    : getTxsRequestData(defaultPageRequest.count,
      defaultPageRequest.pageNumber,
      defaultPageRequest.pageSize);
  return {
    company: selectedCompany, count: pageArg.count, pageNumber: pageArg.pageNumber, pageSize: pageArg.pageSize,
  };
}

export function convertStoragesForFilter(src) {
  const result = [];
  try {
    const data = src.constructor === String ? JSON.parse(src) : src;

    if (data) {
      data.forEach((item) => result.push(item));
    }
  } catch (error) {
    return result;
  }
  return result;
}

export function getFilterData(name, origin) {
  return { value: name, label: name, origin };
}

export function convertDataForFilter(src) {
  const result = [];
  try {
    const data = src.constructor === String ? JSON.parse(src) : src;

    if (data) {
      data.forEach((item) => result.push({ value: item.value, label: item.label }));
    }
  } catch (error) {
    return result;
  }
  return result;
}

export function convertCompanyDtoForFilter(companyDto) {
  const result = [];
  if (companyDto) {
    companyDto.forEach((item) => result.push(getFilterData(item.alias, item)));
  }
  return result;
}

export function convertMasterDtoForFilter(masterDto) {
  const result = [];
  if (masterDto) {
    masterDto.forEach((item) => result.push(getFilterData(item.name, item)));
  }
  return result;
}

export function convertCurrencyDtoForFilter(currencyDto) {
  const result = [];
  if (currencyDto) {
    currencyDto.forEach((item) => result.push(getFilterData(item.currency.toUpperCase(), item)));
  }
  return result;
}

export function convertTokenDtoForFilter(tokenDto) {
  const result = [];
  if (tokenDto) {
    tokenDto.forEach((item) => result.push(getFilterData(item.token.toUpperCase(), item)));
  }
  return result;
}

export function convertDateToString(str) {
  const ms = Date.parse(str);

  if (Number.isNaN(ms)) {
    return str;
  }

  const date = new Date(ms);
  return `${(`0${date.getDate()}`).slice(-2)}.${(`0${date.getMonth() + 1}`).slice(-2)}.${date.getFullYear()}`;
}

export function getConvertingNumber(number) {
  if (!number) return 0;

  if (Math.abs(number) < 1.0) {
    const e = parseInt(number.toString().split('e-')[1]);
    if (e) {
      number *= 10 ** e - 1;
      number = `0.${(new Array(e)).join('0')}${number.toString().substring(2)}`;
    }
  } else {
    let e = parseInt(number.toString().split('+')[1]);
    if (e > 20) {
      e -= 20;
      number /= 10 ** e;
      number += (new Array(e + 1)).join('0');
    }
  }
  return number;
}

export function currenciesComparator(a, b) {
  const currencyA = a.currency.toUpperCase();
  const currencyB = b.currency.toUpperCase();

  let comparison = 0;
  if (currencyA > currencyB) {
    comparison = 1;
  } else if (currencyA < currencyB) {
    comparison = -1;
  }
  return comparison;
}

export function mastersComparator(a, b) {
  const masterA = a.name.toUpperCase();
  const masterB = b.name.toUpperCase();

  let comparison = 0;
  if (masterA > masterB) {
    comparison = 1;
  } else if (masterA < masterB) {
    comparison = -1;
  }
  return comparison;
}

export function companiesComparator(a, b) {
  const companyA = a.alias.toUpperCase();
  const companyB = b.alias.toUpperCase();

  let comparison = 0;
  if (companyA > companyB) {
    comparison = 1;
  } else if (companyA < companyB) {
    comparison = -1;
  }
  return comparison;
}

export function filteredByToken(data) {
  const tokens = [];
  data.forEach((el) => {
    if (el.tokens.length) {
      el.tokens.forEach((token) => {
        if (token) {
          tokens.push({ token });
        }
      });
    }
  });
  return tokens;
}

export function getTableOptions() {
  return {
    firstPageText: 'First',
    prePageText: 'Back',
    nextPageText: 'Next',
    lastPageText: 'Last',
    nextPageTitle: 'First page',
    prePageTitle: 'Pre page',
    firstPageTitle: 'Next page',
    lastPageTitle: 'Last page',
  };
}

export function getTableColumns() {
  return [
    {
      dataField: 'date', text: 'date', headerAlign: 'center', headerStyle: () => ({ width: '9.8em', color: '#79838a' }),
    },
    {
      dataField: 'addressto', text: 'addressto', headerAlign: 'center', headerStyle: () => ({ width: '18.0em', color: '#79838a' }),
    },
    {
      dataField: 'status', text: 'status', headerAlign: 'center', headerStyle: () => ({ width: '5.5em', color: '#79838a' }),
    },
    {
      dataField: 'storageDirection', text: 'direction', headerAlign: 'center', headerStyle: () => ({ width: '5.5em', color: '#79838a' }),
    },
    {
      dataField: 'currency', text: 'currency', headerAlign: 'center', headerStyle: () => ({ width: '5.5em', color: '#79838a' }),
    },
    {
      dataField: 'masterKeyName',
      text: 'master',
      headerAlign: 'center',
      classes: 'tbl-master',
      headerStyle: () => ({ width: '7.5em', color: '#79838a' }),
    },
    {
      dataField: 'hashTx', text: 'hash', headerAlign: 'center', headerStyle: () => ({ width: '18.0em', color: '#79838a' }),
    },
    {
      dataField: 'token', text: 'token', headerAlign: 'center', headerStyle: () => ({ width: '6.5em', color: '#79838a' }),
    },
    {
      dataField: 'amount', text: 'amount', headerAlign: 'center', headerStyle: () => ({ width: '12.5em', color: '#79838a' }),
    },
    {
      dataField: 'tag', text: 'tag', headerAlign: 'center', headerStyle: () => ({ width: '7.1em', color: '#79838a' }),
    },
    {
      dataField: 'fee', text: 'fee', headerAlign: 'center', headerStyle: () => ({ width: '11.5em', color: '#79838a' }),
    },
    {
      dataField: 'indexUtxo',
      text: 'utxo',
      headerAlign: 'center',
      headerTitle: () => 'Index Utxo',
      headerStyle: () => ({ width: '4.0em', color: '#79838a', peddingLeft: '6px' }),
    },
  ];
}



function getShortMasterName(masterKeyName) {
  return (
    <div title={masterKeyName}>
      { masterKeyName.length > 16 ? `${masterKeyName.slice(0, 12)}...` : masterKeyName }
    </div>
  );
}

function sortTxsByDate(res) {
  if (!res) {
    return;
  }
  res.sort((a, b) => b.date - a.date);
}

function convertDate(res) {
  for (let i = 0; i < res.length; i++) {
    res[i].date = new Date(res[i].date).toLocaleString(locale);
  }
  return res;
}



export function isInRange(accByMasters, startFilterDate, endFilterDate) {
  if (startFilterDate.length === 0) {
    startFilterDate = new Date(3600 * 24 * 1000);
  }

  if (startFilterDate && (!endFilterDate || endFilterDate.length === 0)) {
    endFilterDate = new Date();
  }

  if (!endFilterDate || endFilterDate.length === 0) {
    return accByMasters;
  }

  if (startFilterDate.constructor === String) {
    startFilterDate = new Date(startFilterDate);
  }

  if (endFilterDate.constructor === String) {
    endFilterDate = new Date(endFilterDate);
  }

  startFilterDate.setHours(0, 0, 0, 0);
  const startTimeStamp = startFilterDate.getTime();
  endFilterDate.setHours(23, 59, 59, 999);
  let endTimeStamp = endFilterDate.getTime();

  if (endTimeStamp < startTimeStamp) {
    endTimeStamp = startTimeStamp;
  }


  return accByMasters;
}

export function clearDuplicate(array) {
  return [...new Set(array.map((s) => JSON.stringify(s)))].map((s) => JSON.parse(s));
}

export function restoreCompanyFilterSettings(src, aliasToFind) {
  const companiesArr = JSON.parse(src);

  for (let i = 0; i < companiesArr.length; i++) {
    if (companiesArr[i].alias === aliasToFind) {
      return getFilterData(aliasToFind, { id: companiesArr[i].id, alias: companiesArr[i].alias });
    }
  }
  return null;
}

export function getFiltersOptions(storage, currency, masters, tokens) {
  return [
    { name: 'storage', value: convertStoragesForFilter(storage) },
    { name: 'currency', value: convertDataForFilter(currency) },
    { name: 'master', value: convertDataForFilter(masters) },
    { name: 'token', value: convertDataForFilter(tokens) },
  ];
}

export function getShortValue(value, start = 4, end = 2) {
  if (value) {
    const stringValue = value.toString();

    if (stringValue.length > 6) {
      return `${stringValue.substr(0, start)}...${stringValue.substr(stringValue.length - end, end)}`;
    }
    return stringValue;
  }

  return value;
}

export class UserSettings {
  constructor(decoded, token, strtoken, companies, currencies, masters, master, txs, txsall,
    companycurrent, storagescurrent, currenciescurrent, masterscurrent, datepickerstart,
    datepickerend, tabcurrent, txsselected, lastbutton) {
    this.auth = decoded;
    this.token = token;
    this.strtoken = strtoken;
    this.companies = companies;
    this.currencies = currencies;
    this.storages = { cold: 'cold', hot: 'hot' };
    this.masters = masters;
    this.master = master;
    this.txsall = txsall;
    this.companyfilterselection = companycurrent;
    this.storagefilterselection = storagescurrent;
    this.currencyfilterselection = currenciescurrent;
    this.mastersfilterselection = masterscurrent;
    this.datepickerstartselection = datepickerstart;
    this.datepickerendselection = datepickerend;
    this.tabselection = tabcurrent;
    this.txsselection = txsselected;
    this.lastpressedbutton = lastbutton;
  }

  static getUserSettings() {
    return sessionStorage.getItem('userSettings');
  }


  static checkToken(decoded) {
    let result = false;
    let userSettings;
    try {
      const strg = sessionStorage.getItem('userSettings');
      userSettings = JSON.parse(strg);
      if (!userSettings || userSettings === undefined || userSettings === null) {
        const newSettings = new UserSettings(decoded);
        sessionStorage.setItem('userSettings', JSON.stringify(newSettings));
      }
    } catch (err) {
      const newSettings = new UserSettings(decoded);
      sessionStorage.setItem('userSettings', JSON.stringify(newSettings));
    }

    if (userSettings) {
      result = (userSettings.auth.firstName === decoded.firstName && userSettings.auth.lastName === decoded.lastName);
      if (!result) {
        userSettings.companyfilterselection = undefined;
        userSettings.storagefilterselection = undefined;
        userSettings.currencyfilterselection = undefined;
        userSettings.mastersfilterselection = undefined;
        userSettings.datepickerstartselection = undefined;
        userSettings.datepickerendselection = undefined;
        userSettings.tabselection = undefined;
        userSettings.txsselection = undefined;
      }
    }
    return result;
  }

  static setStrTokenTokenDecoded(decoded, token, strToken) {
    const userSettings = JSON.parse(sessionStorage.getItem('userSettings'));
    userSettings.auth = decoded;
    userSettings.token = token;
    userSettings.strtoken = strToken;
    sessionStorage.setItem('userSettings', JSON.stringify(userSettings));
    return userSettings;
  }

  static setLastPressedButton(lastBtnName) {
    const userSettings = JSON.parse(sessionStorage.getItem('userSettings'));
    if (!userSettings) return;
    userSettings.lastpressedbutton = JSON.stringify(lastBtnName);
    sessionStorage.setItem('userSettings', JSON.stringify(userSettings));
  }

  static clearSessionStorage() {
    sessionStorage.clear();
  }

}
