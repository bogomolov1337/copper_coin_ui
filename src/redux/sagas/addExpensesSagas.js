import axios from 'axios';
import {call, put, takeEvery} from 'redux-saga/effects';
import actions from '../actions';
import * as message from '../../constants/messages';
import urlUtils from '../../constants/base';
import {setConsumption, setExpenditureWindowState} from "../actionsCreator";
import {authRequest} from "./authSaga";

export async function regRequest(url, payload) {
    console.log(payload)
    return axios.post(url, payload)
        .then((res) => res).catch((err) => err.response);
}

export function* regUser(action) {
    if (!action || !action.payload) {
        return;
    }

    try {
        const response = yield call(regRequest, `${urlUtils.BACK_END_URL}/general`, action.payload);
        yield call(processResp, response);
        const responseCONSUMPTION = yield call(authRequest, `${urlUtils.BACK_END_URL}/general`, {
            userId: action.payload.userId,
            cmd: 'GET_ALL_USER_CONSUMPTION'
        });
        yield put(setConsumption(responseCONSUMPTION.data));
        yield call(setExpenditureWindowState);

    } catch (error) {
        yield put({type: actions.GET_ERRORS, payload: error});
    }
}

export const processResp = (r) => {
    switch (r.status) {
        case 200:
            break;
        case 202:
            throw new Error(message.invalidToken);
        case 400:
            throw new Error(r.data);
        case 401:
        case 403:
        case 404:
            throw new Error(r.data);
        case 409:
            throw new Error(r.data);
        default:
            throw new Error(message.errorServer);
    }
};

export default function* watchExpManager() {
    yield takeEvery(actions.CREATE_EXPENSES, regUser);

}
