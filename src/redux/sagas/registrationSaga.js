import axios from 'axios';
import {call, put, takeEvery} from 'redux-saga/effects';
import actions from '../actions';
import * as message from '../../constants/messages';
import urlUtils from '../../constants/base';
import {setRegMessage, setRegWindowState} from "../actionsCreator";

export async function regRequest(url, payload) {
    console.log(payload)
    return axios.post(url, payload)
        .then((res) => res).catch((err) => err.response);
}

export function* regUser(action) {
    if (!action || !action.payload) {
        return;
    }

    try {
        const response = yield call(regRequest, `${urlUtils.BACK_END_URL}/registration`, action.payload);
        yield call(processResp, response);
        yield put(setRegWindowState(true));
        yield put(setRegMessage(response.data));
    } catch (error) {
        yield put({type: actions.GET_ERRORS, payload: error});
    }
}

export const processResp = (r) => {
    switch (r.status) {
        case 200:
            break;
        case 202:
            throw new Error(message.invalidToken);
        case 400:
            throw new Error(r.data);
        case 401:
        case 403:
        case 404:
            throw new Error(r.data);
        case 409:
            throw new Error(r.data);
        default:
            throw new Error(message.errorServer);
    }
};

export default function* watchRegManager() {
    yield takeEvery(actions.USER_REGISTRATION, regUser);

}
