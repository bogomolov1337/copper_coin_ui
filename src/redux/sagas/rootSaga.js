import {all, call} from 'redux-saga/effects';
import watchAuthManager from './authSaga';
import watchRegManager from "./registrationSaga";
import watchResetManager from "./forgotPasswordSaga";
import watchCategoryManager from "./categorySaga";
import watchExpManager from "./addExpensesSagas";

const sagasList = [
    watchAuthManager,
    watchRegManager,
    watchResetManager,
    watchCategoryManager,
    watchExpManager,
];

export default function* watchRootSaga() {
    yield all(sagasList.map((saga) => call(saga)));
}
