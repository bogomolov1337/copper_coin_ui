import axios from 'axios';
import {takeEvery, put, call} from 'redux-saga/effects';
import jwtDecode from 'jwt-decode';
import actions from '../actions';
import * as message from '../../constants/messages';
import urlUtils from '../../constants/base';
import {setAUTH, setGet2faWindowState, setSuccessForgotWindowState} from "../actionsCreator";

export async function authRequest(url, user) {
  return axios.post(url, user)
    .then((res) => res).catch((err) => err.response);
}

export function* loginUser(action) {
    console.log(action.payload + 'forgot')

    if (!action || !action.payload) {
        return;
    }
    try {
        const response = yield call(authRequest, `${urlUtils.BACK_END_URL}/forgot`, action.payload);
        yield call(processResp, response);
        yield put(setSuccessForgotWindowState(true))
        yield put(setGet2faWindowState(false))
    } catch (error) {
        yield put({type: actions.GET_ERRORS, payload: error});
    }
}

export const processResp = (r) => {
    switch (r.status) {
        case 200:
            break;
        case 202:
            throw new Error(message.invalidToken);
        case 400:
            throw new Error(r.data);
        case 401:
            throw new Error(r.data);
        case 403:
        case 404:
            throw new Error(r.data);
        case 409:
            throw new Error(r.data);
        default:
            throw new Error(message.errorServer);
    }
};

export default function* watchResetManager() {
    yield takeEvery(actions.RESET_PASSWORD_USER, loginUser);

}
