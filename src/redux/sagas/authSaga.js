import axios from 'axios';
import {apply, call, put, takeEvery} from 'redux-saga/effects';
import jwtDecode from 'jwt-decode';
import setAuthToken from '../../utils/setAuthToken';
import actions from '../actions';
import * as message from '../../constants/messages';
import urlUtils from '../../constants/base';
import {setAUTH, setCategory, setConsumption, setGet2faWindowState} from "../actionsCreator";

export async function authRequest(url, user) {
    return axios.post(url, user)
        .then((res) => res).catch((err) => err.response);
}

export function* loginUser(action) {
    console.log(action.payload)

    if (!action || !action.payload) {
        return;
    }
    try {
        const response = yield call(authRequest, `${urlUtils.BACK_END_URL}/authorization`, action.payload);
        yield call(processResp, response);
        const strToken = yield call(JSON.stringify, response.data);
        sessionStorage.setItem('jwtToken', strToken);
        yield call(setAuthToken, strToken);
        const decoded = yield call(jwtDecode, strToken);
        yield put(setCurrentUser(decoded));
        yield put(setAUTH(true));
        const responseCONSUMPTION = yield call(authRequest, `${urlUtils.BACK_END_URL}/general`, {
            userId: decoded.sub,
            cmd: 'GET_ALL_USER_CONSUMPTION'
        });
        const responseCategory = yield call(authRequest, `${urlUtils.BACK_END_URL}/general`, {
            userId: decoded.sub,
            cmd: 'GET_ALL_USER_CATEGORY'
        });
        yield put(setCategory(responseCategory.data));
        yield put(setConsumption(responseCONSUMPTION.data));
    } catch (error) {
        yield put({type: actions.GET_ERRORS, payload: error});
    }
}

export function* logoutUser() {
    console.log('logout')
    yield apply(sessionStorage, sessionStorage.clear);
    yield call(setAuthToken, false);
    yield put(setLogout(false))
    yield put(setGet2faWindowState(false))
    yield put({type: actions.GET_ERRORS, payload: ''})
    yield put(setCurrentUser({}));
}

export const setCurrentUser = (decoded) => ({
    type: actions.SET_CURRENT_USER,
    payload: decoded,
});

export const setLogout = (b) => ({
    type: actions.SET_AUTH,
    payload: b,
});

export const processResp = (r) => {
    switch (r.status) {
        case 200:
            break;
        case 202:
            throw new Error(message.invalidToken);
        case 400:
            throw new Error(r.data);
        case 401:
            throw new Error(r.data);
        case 403:
        case 404:
            throw new Error(r.data);
        case 409:
            throw new Error(r.data);
        default:
            throw new Error(message.errorServer);
    }
};

export default function* watchAuthManager() {
    yield takeEvery(actions.USER_LOGIN, loginUser);
    yield takeEvery(actions.USER_LOGOUT, logoutUser);

}
