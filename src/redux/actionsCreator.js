import actions from './actions';

export const setUser = (payload) => ({type: actions.USER_LOADING, payload});
export const resetUser = (payload) => ({type: actions.RESET_PASSWORD_USER, payload});
export const resetCurrentUser = (payload) => ({type: actions.SET_CURRENT_USER, payload});
export const setErrorMessage = (payload) => ({type: actions.GET_ERRORS, payload});
export const loginUser = (payload) => ({type: actions.USER_LOGIN, payload});
export const logoutUser = () => ({type: actions.USER_LOGOUT});

export const setGet2faWindowState = (payload) => ({type: actions.IS_OPEN_2fa_MODAL_WINDOW, payload});
export const setExpenditureWindowState = (payload) => ({type: actions.IS_OPEN_EXPENDITURE_MODAL_WINDOW, payload});
export const setAddCategoryWindowState = (payload) => ({type: actions.IS_OPEN_ADD_CATEGORY_MODAL_WINDOW, payload});
export const setRegWindowState = (payload) => ({type: actions.IS_OPEN_REG_MODAL_WINDOW, payload});
export const setSuccessForgotWindowState = (payload) => ({type: actions.IS_OPEN_SUCCESS_FORGOT_MODAL_WINDOW, payload});
export const setLogoutWindowState = (payload) => ({type: actions.USER_LOGOUT_OPEN_WINDOW, payload});
export const setPremiumWindowState = (payload) => ({type: actions.IS_OPEN_PREMIUM_MODAL_WINDOW, payload});
export const setPersonalDataWindowState = (payload) => ({type: actions.IS_PERSONAL_DATA_MODAL_WINDOW, payload});
export const setRegMessage = (payload) => ({type: actions.SET_MESSAGE_2FA_CODE, payload});
export const closeAllModal = (payload) => ({type: actions.CLOSE_ALL_MODAL, payload});
export const showSmsPinError = (payload) => ({type: actions.SET_ERROR_MESSAGE, payload});
export const changeBorderColor = (payload) => ({type: actions.SET_ERROR_MASSAGE_BORDER_COLOR, payload});
export const changeMessageColor = (payload) => ({type: actions.SET_ERROR_MASSAGE_COLOR, payload});
export const regUser = (payload) => ({type: actions.USER_REGISTRATION, payload});
export const setAUTH = (payload) => ({type: actions.SET_AUTH, payload});
export const setDatePickerStartDate = (payload) => ({ type: actions.SET_START_DATE_FOR_TXS_FILTERING_STORE, payload });
export const setDatePickerEndDate = (payload) => ({ type: actions.SET_END_DATE_FOR_TXS_FILTERING_STORE, payload });
export const setConsumption = (payload) => ({ type: actions.SET_ALL_USER_CONSUMPTION, payload });
export const setCategory = (payload) => ({ type: actions.SET_ALL_USER_CATEGORY, payload });
export const createCategory = (payload) => ({ type: actions.CREATE_CATEGORY, payload });
export const createExpenses = (payload) => ({ type: actions.CREATE_EXPENSES, payload });
