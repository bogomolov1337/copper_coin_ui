import actions from '../actions';

const initialState = {
    errorMessage: "",
    messageColor: "",
    borderColor: "",
};

export default function (state = initialState, action) {
    switch (action.type) {
        case actions.SET_ERROR_MESSAGE:
            return {
                ...state,
                errorMessage: action.payload,
            };
        case actions.SET_ERROR_MASSAGE_BORDER_COLOR:
            return {
                ...state,
                borderColor: action.payload,
            };
        case actions.SET_ERROR_MASSAGE_COLOR:
            return {
                ...state,
                messageColor: action.payload,
            };
        default:
            return state;
    }
}
