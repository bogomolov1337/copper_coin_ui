import actions from '../actions';

const isEmpty = require('ast-is-empty');

const initialState = {
    isAuth: false,
    user: [],
    category: [],
    CONSUMPTION: []
};

export default (state = initialState, action) => {
    switch (action.type) {

        case actions.SET_CURRENT_USER:
            return {
                ...state,
                user: action.payload,
            };
        case actions.RESET_PASSWORD_USER:
            return {
                ...state,
                user: action.payload,
            };
        case actions.SET_AUTH:
            return {
                ...state,
                isAuth: action.payload,
            };

            case actions.SET_ALL_USER_CATEGORY:
            return {
                ...state,
                category: action.payload,
            };

            case actions.SET_ALL_USER_CONSUMPTION:
            return {
                ...state,
                CONSUMPTION: action.payload,
            };
        default:
            return state;
    }
};
