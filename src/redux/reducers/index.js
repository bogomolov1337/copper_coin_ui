import {combineReducers} from 'redux';
import authReducer from './authReducers';
import jwtTokenReducer from './jwtTokenReducer';
import modalWindowReducer from './modalWindowReducer';
import errorMessageReducer from './errorMessageReducer';
import errorReducer from './errorReducers';
import messageReducer from "./messageReducer";
import datePickerStartDateReducer from "./datePickerStartDateReducer";
import datePickerEndDateReducer from "./datePickerEndDateReducer";

export default combineReducers({
    auth: authReducer,
    errors: errorReducer,
    jwttokens: jwtTokenReducer,
    modalWindowState: modalWindowReducer,
    errorMassageState: errorMessageReducer,
    messageState: messageReducer,
    startdate: datePickerStartDateReducer,
    enddate: datePickerEndDateReducer,
});
