import actions from '../actions';

const initialState = {
    isOpenModal2FaPinWindow: false,
    isOpenModalRegWindow: false,
    isOpenModalSuccessForgotWindow: false,
    isLogoutOpen: false,
    isPremiumOpen: false,
    isPersonalDataOpen: false,
    isExpenditureDataOpen: false,
    isAadCategoryOpen: false,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case actions.IS_OPEN_2fa_MODAL_WINDOW:
            return {
                ...state,
                isOpenModal2FaPinWindow: action.payload,
            };

        case actions.IS_OPEN_REG_MODAL_WINDOW:
            return {
                ...state,
                isOpenModalRegWindow: action.payload,
            };

        case actions.IS_OPEN_SUCCESS_FORGOT_MODAL_WINDOW:
            return {
                ...state,
                isOpenModalSuccessForgotWindow: action.payload,
            };

        case actions.USER_LOGOUT_OPEN_WINDOW:
            return {
                ...state,
                isLogoutOpen: action.payload,
            };

        case actions.IS_OPEN_PREMIUM_MODAL_WINDOW:
            return {
                ...state,
                isPremiumOpen: action.payload,
            };

        case actions.IS_PERSONAL_DATA_MODAL_WINDOW:
            return {
                ...state,
                isPersonalDataOpen: action.payload,
            };

        case actions.IS_OPEN_EXPENDITURE_MODAL_WINDOW:
            return {
                ...state,
                isExpenditureDataOpen: action.payload,
            };

        case actions.IS_OPEN_ADD_CATEGORY_MODAL_WINDOW:
            return {
                ...state,
                isAadCategoryOpen: action.payload,
            };

        case actions.CLOSE_ALL_MODAL:
            return {
                ...state
            };
        default:
            return state;
    }
}
