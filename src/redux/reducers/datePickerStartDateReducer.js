import actions from '../actions';

const initialState = {
  startdate: [],
  tempstartdate: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case actions.SET_START_DATE_FOR_TXS_FILTERING_STORE:
      return {
        ...state,
        startdate: action.payload,
      };
    case actions.SET_TEMP_START_DATE_FOR_TXS_FILTERING_STORE:
      return {
        ...state,
        tempstartdate: action.payload,
      };
    default:
      return state;
  }
}
