import actions from '../actions';

const initialState = {
    registrationMessage: ''
};

export default function (state = initialState, action) {
    switch (action.type) {
        case actions.SET_MESSAGE_2FA_CODE:
            return {
                ...state,
                registrationMessage: action.payload,
            };

        default:
            return state;
    }
}
