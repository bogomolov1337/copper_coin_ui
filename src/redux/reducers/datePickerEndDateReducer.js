import actions from '../actions';

const initialState = {
  enddate: [],
  tempenddate: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case actions.SET_END_DATE_FOR_TXS_FILTERING_STORE:
      return {
        ...state,
        enddate: action.payload,
      };
    case actions.SET_TEMP_END_DATE_FOR_TXS_FILTERING_STORE:
      return {
        ...state,
        tempenddate: action.payload,
      };
    default:
      return state;
  }
}
