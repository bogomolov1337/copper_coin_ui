import actions from '../actions';

const initialState = {
  jwttokens: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case actions.SET_REFRESH_TOKEN_STORE:
      return {
        ...state,
        jwttokens: action.payload,
      };
    default:
      return state;
  }
}
