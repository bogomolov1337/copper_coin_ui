import React, {useContext, useState} from 'react';
import {UserContext} from "../userContext/UserContext";
import {Button, ButtonReg, ButtonRegistrContainer, Company, ItemsContainer, RegTitle, Wrapper} from "../login/style";
import logo from "../../logo.png";
import {NavLink} from "react-router-dom";
import {Body, ButtonContainer, ButtonRegistr, ErrorText, Label, Text} from "./style";
import FormInput from "./FormInput/FormInput";
import ModalRegistrationWindow from "./ModalWindow/ModalRegistrationWindow";


const Registration = () => {
    const [userName, setUserName] = useState('');
    const [pass, setPass] = useState('');
    const [confPass, setConfPass] = useState('');
    const [lastName, setLastName] = useState('');
    const [login, setLogin] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');

    const [passwordColor, setPasswordColor] = useState('#fff');
    const [firstNameColor, setFirstNameColor] = useState('#fff');
    const [lastNameColor, setLastNameColor] = useState('#fff');
    const [loginColor, setLoginColor] = useState('#fff');
    const [emailColor, setEmailColor] = useState('#fff');
    const [phoneColor, setPhoneColor] = useState('#fff');

    const [passwordErrorMessage, setPasswordErrorMessage] = useState('');
    const [firstNameErrorMessage, setFirstNameErrorMessage] = useState('');
    const [lastNameErrorMessage, setLastNameErrorMessage] = useState('');
    const [loginErrorMessage, setLoginErrorMessage] = useState('');
    const [emailErrorMessage, setEmailErrorMessage] = useState('');
    const [phoneErrorMessage, setPhoneErrorMessage] = useState('');
    const {
        actions: {regUser, showMessageError},
        selectors: {errorMessage, isOpenModalRegWindow}
    } = useContext(UserContext);

    const handleClick = () => {
        let isError = false;
        if (pass !== confPass || pass.length !== confPass.length || pass.length < 4 || confPass.length < 4) {
            setPasswordColor('red')
            showMessageError("*Password mismatch. Try again");
            isError = true;
        } else {
            setPasswordColor('#fff')
            showMessageError("");
        }

        if (userName.length < 2 || userName.length > 64) {
            setFirstNameErrorMessage('*Invalid First name. Try again')
            setFirstNameColor('red')
            isError = true;
        } else {
            setFirstNameErrorMessage('')
            setFirstNameColor('#fff')
        }

        if (lastName.length < 2 || lastName.length > 64) {
            setLastNameErrorMessage('*Invalid Last name name. Try again')
            setLastNameColor('red')
            isError = true;
        } else {
            setLastNameErrorMessage('')
            setLastNameColor('#fff')
        }

        if (login.length < 6 || login.length > 40) {
            setLoginErrorMessage('*Invalid Login. Try again')
            setLoginColor('red')
            isError = true;
        } else {
            setLoginErrorMessage('')
            setLoginColor('#fff')
        }

        if (email.length < 6 || email.length > 40) {
            setEmailErrorMessage('*Invalid  Email. Try again')
            setEmailColor('red')
            isError = true;
        } else {
            setEmailErrorMessage('')
            setEmailColor('#fff')
        }

        if (phone.length < 10 || phone.length > 12) {
            setPhoneErrorMessage('*Invalid  Phone. Try again')
            setPhoneColor('red')
            isError = true;
        } else {
            setPhoneErrorMessage('')
            setPhoneColor('#fff')
        }

        if (isError) {
            return;
        }

        const payload = {
            firstName: userName,
            lastName: lastName,
            login: login,
            pass: pass,
            email: email,
            phone: phone
        };

        setPasswordColor('#fff')
        setLoginColor('#fff')
        setFirstNameColor('#fff')
        setLastNameColor('#fff')
        setEmailColor('#fff')
        setPhoneColor('#fff')
        showMessageError("");

        regUser(payload)
    }

    const onLoginChange = (e) => {
        setLogin(e.target.value.trim());
    };

    const onEmailChange = (e) => {
        setEmail(e.target.value.trim());
    };

    const onPhoneChange = (e) => {
        setPhone(e.target.value.trim());
    };

    const onFNameChange = (e) => {
        setUserName(e.target.value.trim());
    };

    const onLNameChange = (e) => {
        setLastName(e.target.value.trim());
    };

    const onPasswordChange = (e) => {
        setPass(e.target.value.trim());
    };

    const onConfirmPasswordChange = (e) => {
        setConfPass(e.target.value.trim());
    };

    const showError = (errorMessage) => {
        let messages = errorMessage.message.split(',');
        let finalString = '';
        for (let i = 0; i < messages.length; i++) {
            if (i === messages.length - 1) {
                finalString += messages[i]
                break;
            }
            finalString += messages[i] + ', '
        }
        return finalString;
    }

    return (
        <Wrapper data-at="wrapper">

            <ItemsContainer.Company>
                <Company.Logo src={logo}/>
                <Company.Name>Copper coin - don't waste your money</Company.Name>
                <RegTitle>Do you have account?</RegTitle>
                <ButtonRegistrContainer>
                    <NavLink to='/login'>
                        <ButtonReg id={'registration.link_to_login'}>
                            <Button.Label children={'Authorization'}/>
                        </ButtonReg>
                    </NavLink>
                </ButtonRegistrContainer>
            </ItemsContainer.Company>
            <Body>
                <Text>
                    Please complete all fields below.
                </Text>
                <FormInput
                    id={'registration.first_name_input'}
                    max={64}
                    onChange={onFNameChange}
                    labelText={'First name'}
                    styleValidation={firstNameColor}
                    message={firstNameErrorMessage}
                    messageDisplay={'flex'}
                />

                <FormInput
                    id={'registration.last_name_input'}
                    max={64}
                    onChange={onLNameChange}
                    labelText={'Last name'}
                    styleValidation={lastNameColor}
                    message={lastNameErrorMessage}
                    messageDisplay={'flex'}
                />

                <FormInput
                    id={'registration.login_input'}
                    max={40}
                    onChange={onLoginChange}
                    labelText={'Login'}
                    styleValidation={loginColor}
                    message={loginErrorMessage}
                    messageDisplay={'flex'}
                />

                <FormInput
                    id={'registration.email_input'}
                    max={40}
                    onChange={onEmailChange}
                    labelText={'Email'}
                    styleValidation={emailColor}
                    message={emailErrorMessage}
                    messageDisplay={'flex'}
                />

                <FormInput
                    id={'registration.phone_input'}
                    max={12}
                    onChange={onPhoneChange}
                    labelText={'Phone'}
                    inputType={'number'}
                    styleValidation={phoneColor}
                    message={phoneErrorMessage}
                    messageDisplay={'flex'}
                />

                <FormInput
                    id={'registration.password_input'}
                    onChange={onPasswordChange}
                    labelText={'Password'}
                    inputType="password"
                    styleValidation={passwordColor}
                    message={'message'}
                    messageDisplay={'none'}
                />

                <FormInput
                    id={'registration.confirm_password_input'}
                    onChange={onConfirmPasswordChange}
                    labelText={'Confirm password'}
                    inputType="password"
                    styleValidation={passwordColor}
                    message={'message'}
                    messageDisplay={'none'}
                />

                <ErrorText>{typeof (errorMessage) === "string" ? errorMessage : showError(errorMessage)}</ErrorText>
                <ButtonContainer>
                    <ButtonRegistr id={'registration.registration_btn'} onClick={handleClick}>
                        <Label children={'Registration'}/>
                    </ButtonRegistr>
                </ButtonContainer>
                {isOpenModalRegWindow && <ModalRegistrationWindow/>}
            </Body>
        </Wrapper>
    );

};


export default Registration;
