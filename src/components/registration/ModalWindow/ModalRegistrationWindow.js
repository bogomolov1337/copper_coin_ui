import React, {useContext, useEffect} from 'react';
import {FooterText, LabelText, ModalWindow, ModalWindowBody, ResponseText,} from './style';
import {UserContext} from "../../userContext/UserContext";
import OkButtonContainer from "./ApplyButton/OkButtonContainer";

const ModalRegistrationWindow = (props) => {

    const {
        actions: {onRegistrationModalChange},
        selectors: {
            regMessage
        },
    } = useContext(UserContext);

    useEffect(() => {
        const close = (e) => {
            if (e.keyCode === 27) {
                onRegistrationModalChange();
            }
        };
        window.addEventListener('keydown', close);
        return () => window.removeEventListener('keydown', close);
    }, []);

    return (
        <ModalWindow>
            <ModalWindow.Blocker/>
            <ModalWindowBody>
                <ModalWindowBody.Header>
                    <ModalWindowBody.Header.Title>Successful registration</ModalWindowBody.Header.Title>
                    <ModalWindowBody.Header.CloseButton onClick={onRegistrationModalChange} title="Close">
                        <CloseIcon
                            width="14"
                            height="14"
                            viewBox="0 0 14 14"
                        />
                    </ModalWindowBody.Header.CloseButton>
                </ModalWindowBody.Header>
                <LabelText>Registration completed successfully. Your personal code:</LabelText>
                <ModalWindowBody.Container>
                    <ResponseText>{regMessage}</ResponseText>
                </ModalWindowBody.Container>
                <FooterText>Enter this code to Google authenticator.</FooterText>
                <ModalWindowBody.Footer>
                    <OkButtonContainer></OkButtonContainer>
                </ModalWindowBody.Footer>


            </ModalWindowBody>
        </ModalWindow>
    );
};

export default ModalRegistrationWindow;

export function CloseIcon(props) {
    return (
        <svg {...props}>
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M7.00008 8.41429L12.293 13.7072L13.7072 12.293L8.41429 7.00008L13.7072 1.70718L12.293 0.292969L7.00008
         5.58586L1.70718 0.292969L0.292969 1.70718L5.58586 7.00008L0.292969 12.293L1.70718 13.7072L7.00008 8.41429Z"
                fill="#79838A"
            />
        </svg>
    );
}