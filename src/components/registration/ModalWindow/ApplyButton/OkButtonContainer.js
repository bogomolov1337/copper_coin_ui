import React, {useContext} from 'react';
import {OkButton} from './style';
import {NavLink} from "react-router-dom";
import {UserContext} from "../../../userContext/UserContext";

const OkButtonContainer = (props) => {
    const {
        actions: { onRegistrationModalChange, resetRegCode },
        selectors: {
        },
    } = useContext(UserContext);

    const handleClick = () => {
        onRegistrationModalChange();
        resetRegCode();
    };

    return (
        <NavLink to='/login'>
            <OkButton onClick={handleClick}>Ok</OkButton>
        </NavLink>
    );
};

export default OkButtonContainer;
