import styled from 'styled-components';

export const OkButton = styled.div`
    width: 80px;
    height: 50px;
    border: 1px solid ${(props) => props.borderColor};
    box-sizing: border-box;
    border-radius: 4px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 16px;
    color:  #FF9900;
    cursor: pointer;

    &:hover {
        transition: all .2s cubic-bezier(.47,0,.745,.715);
        background:  #ff9900;
        color: #142836;
    }
`;
