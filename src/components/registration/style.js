import styled from "styled-components";

export const Body = styled.div`
    margin-left: auto; 
    margin-right: auto; 
    margin-top: auto; 
    margin-bottom: auto; 
    width: 800px;
    min-height: 500px;
    height: auto;
    background-color: #142836;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    border-radius: 20px;
  
`;

export const ButtonRegistr = styled.button`
    width: 150px;
    height: 30px;
    border-radius: 10px;
    border: 1px solid #FF9900;
    background-color: #142836;
    outline: 0 !important;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #FF9900;
    
    &:hover {
      background-color: #ff9900;
      transition: all .3s cubic-bezier(.47,0,.745,.715);
      color: #142836;
    }
`;

export const Label = styled.label`
    height: 20px;
    display: flex;
    justify-content: center;
    font-size: 18px;
    text-transform: uppercase;
    font-family: 'Roboto', sans-serif;
    cursor: pointer;
`;
export const Text = styled.h1`
    margin-left: 5%; 
    margin-right: auto; 
    margin-bottom: 2%; 
    font-family: "Roboto";
    color: #fff;
    font-size: 14px;
    font-weight: normal;
   
    padding: 0;
    user-select: none;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    
`;

export const ErrorText = styled.h1` 
    height: 20px;
    width: 100%;
    font-family: "Roboto";
    color: red;
    font-size: 14px;
    font-weight: normal;
   
    padding: 0;
    user-select: none;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    
`;


export const ButtonContainer = styled.div`
    width: 150px;
    padding-left: 17px; 
`;