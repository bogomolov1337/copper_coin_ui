import styled from 'styled-components';

const inputHeight = 24;
const iconEyeSize = 24;
const inputPadding = 12;
const labelFontSize = 20;
const inputsFontSize = 20;
const inputBorderWidth = 1;
const inputBorderRadius = 4;
const iconEyeRightPosition = 14;
const iconEyeBottomPosition = 12;
const inputBlockMarginBottom = 20;

export const Wrapper = styled.div`
    width: 100%;
    display: flex;
    margin-top: 10px;

`;

export const InputWrapper = styled.div`
    width: 60%;
    display: flex;
    flex-direction: column;
    margin-left: 15%;
`;

Wrapper.Input = styled.input`
    width: 90%;
    height: ${inputHeight}px;
    padding: 0 ${inputPadding}px;
    outline: none;
    border-radius: ${inputBorderRadius}px;
    font-size: ${inputsFontSize}px;
    border: ${inputBorderWidth}px;
    color: #D3DBE0;
    background-color: #263845;
    font-family: 'Roboto', sans-serif;
    border: 1px solid ${(props) => props.borderColor};
    margin-right: 5%;
    :-webkit-autofill,
    :-webkit-autofill:hover, 
    :-webkit-autofill:focus {
        -webkit-text-fill-color: #808000;
        -webkit-box-shadow: 0 0 0px 1000px #FDF5E6 inset;
        transition: background-color 5000s ease-in-out 0s;
    }
    
    &::-ms-clear, &::-ms-reveal {
      display: none;
    }
`;

Wrapper.iconEye = styled.div`
  position: absolute;
  right: ${iconEyeRightPosition}px;
  bottom: ${iconEyeBottomPosition}px;
  width: ${iconEyeSize}px;
  height: ${iconEyeSize}px;
  background-repeat: no-repeat;
  background-size: contain;
  cursor: pointer;
`;

export const ErrorMsg = styled.div`
    bottom: 7px;
    height: 20px;
    color: #f70303e6;
    width: 100%;
    text-align: center;
    display: ${(props) => props.display}
`;

export const Label = styled.div`
  margin-left: 5%;
  margin-right: auto;
  font-family: 'Roboto', sans-serif;
`;

Label.Text = styled.label`
  color: #79838A;
`;

Wrapper.Label = Label;
