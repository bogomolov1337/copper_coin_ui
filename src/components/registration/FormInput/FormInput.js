import React from 'react';
import {Wrapper, Label, ErrorMsg, InputWrapper} from './style';

const FormInput = (props) => {
    const {
        max,
        min,
        labelText,
        inputType,
        onChange,
        styleValidation,
        message,
        messageDisplay,
        id,
    } = props;
    return (
        <Wrapper data-at="form-input_wrapper">
            <Wrapper.Label data-at="wrapper_label">
                <Label.Text
                    data-at="label_text"
                    children={labelText}
                />
            </Wrapper.Label>
                <InputWrapper>
            <Wrapper.Input
                type={inputType}
                data-at="wrapper_input"
                onChange={onChange}
                borderColor={styleValidation}
                maxLength={max}
                id={id}
            />
            <ErrorMsg display={messageDisplay}>{message}</ErrorMsg>
             </InputWrapper>
        </Wrapper>
    );
};

export default FormInput;
