import styled from "styled-components";
import {CloseButton} from "../../registration/ModalWindow/style";

export const LabelText = styled.div`
    margin-left: 5%; 
    margin-right: auto; 
    font-family: "Roboto";
    color: #fff;
    font-size: 16px;
    font-weight: normal;
    user-select: none;
`;

export const ModalWindow = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 4;
`;

export const Blocker = styled.div`
    position: fixed;
    background: rgba(0, 0, 0, .7);
    width: 100%;
    height: 100%;
`;

export const ModalWindowBody = styled.div`
    width: 600px;
    height: 300px;
    background: #0c202e;
    border-radius: 4px;
    z-index: 1;
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

export const Header = styled.div`
    width: 100%;
    padding: 18px 24px;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    user-select: none;
`;

export const Footer = styled.div`
    padding: 18px 24px;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
`;

ModalWindowBody.Footer = Footer;

ModalWindowBody.Header = Header;

ModalWindowBody.Header.CloseButton = CloseButton;

ModalWindow.Blocker = Blocker;
