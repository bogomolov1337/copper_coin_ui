import React from 'react';
import { useTranslation } from 'react-i18next';
import { StyledInput, StyledLabel, Wrapper } from './style';

const FormGroup = (props) => {
  const { t } = useTranslation();

  const {
    errorData, type, name, handleChange, handleBlur, value, label,maxLength
  } = props;
  return (
    <Wrapper
      errorDataa={errorData && t(`${errorData}`)}
    >
      <StyledLabel htmlFor={name}>
        {label}
        <StyledInput
          type={type}
          name={name}
          onChange={handleChange}
          onBlur={handleBlur}
          value={value}
          maxLength={maxLength}
        />
      </StyledLabel>
    </Wrapper>
  );
};

export default FormGroup;
