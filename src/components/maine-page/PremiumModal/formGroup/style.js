import styled from 'styled-components';

const errorData = (props) => props.errorDataa;

export const StyledInput = styled.input`
  width: 100%;
  margin-top: 8px;
  background-color: #263845;
  border-radius: 4px;
  height: 48px;
  border: none;
  outline: none;
  color: #D3DBE0;
  padding-left: 14px;
`;

export const StyledLabel = styled.label`
  width: 100%;
`;

export const Wrapper = styled.div`
  max-width: 400px;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: left;
  align-items: start;
  outline: none;
  border: none;
  font-size: 14px;
  line-height: 16px;
  color: #D3DBE0;
  font-weight: 500;
  font-family: 'Roboto', sans-serif;
  border-radius: 3px;
  margin-bottom: 20px;
  position: relative;
    &::before {
      content: '${errorData}';
      font-size: 12px;
      line-height: 1;
      font-family: 'Roboto', sans-serif;
      font-weight: 300;
      position: absolute;
      left: 0;
      right: 0;
      bottom: -14px;
      color: red;
      text-align: center;
    }
`;
