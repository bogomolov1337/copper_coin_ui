import React, {useContext} from 'react';
import {OkButton} from './style';
import {UserContext} from "../../../userContext/UserContext";

const OkButtonContainer = (props) => {
    const {
        actions: {},
        selectors: {},
    } = useContext(UserContext);

    const handleClick = () => {

    };

    return (
        <OkButton onClick={handleClick}>Buy</OkButton>
    );
};

export default OkButtonContainer;
