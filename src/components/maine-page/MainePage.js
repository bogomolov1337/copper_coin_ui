import {Body, Content, Footer} from "../style";
import {Company, ItemsContainer} from "../login/style";
import logo from "../../logo.png";
import React, {useContext} from "react";
import UserMenuComboBox from "./UserMenuComboBox/UserMenuComboBox";
import {Header, PremiumButton} from "./style";
import {UserContext} from "../userContext/UserContext";
import PremiumModal from "./PremiumModal/PremiumModal";
import DiagramBlock from "./DiagramMenu/DiagrameMenu";
import ListOfExpenses from "./Footer/ListOfExpenses";
import StatisticOfExpenses from "./Footer/StatisticOfExpenses";
import CentralBlock from "./CentralBlock/CentralBlock";
import DatePickerBlock from "./DateickerBlock/DatePickerBlock";

const MainePage = () => {

    const {
        actions: {onPremiumModalChange},
        selectors: {isPremiumOpen},
    } = useContext(UserContext);

    const handlePremiumClick = () => {
        onPremiumModalChange()
    }

    return (
        <Content>
            <Header>
                <ItemsContainer.Company>
                    <Company.Logo src={logo}/>
                    <Company.Name>Copper coin - don't waste your money</Company.Name>
                </ItemsContainer.Company>
                {isPremiumOpen && <PremiumModal/>}
                <PremiumButton onClick={handlePremiumClick}>Premium account</PremiumButton>
                <UserMenuComboBox/>
            </Header>
            <Body>
                <DiagramBlock/>
                <CentralBlock/>
                <DatePickerBlock/>
            </Body>
            <Footer>
                <ListOfExpenses/>
                <StatisticOfExpenses/>
            </Footer>

        </Content>
    );
}

export default MainePage;