import {
    CentralBlockWrapper,
    Footer,
    Item,
    ItemLabel,
    ItemWrapper,
    Label,
    ProgressBarWrapper,
    UpdateButton,
} from "./style";
import React, {useContext} from "react";
import {ProgressBar} from "react-bootstrap";
import {UserContext} from "../../userContext/UserContext";

const CentralBlock = () => {
    const {
        actions: {setExpenditureWindowState},
        selectors: {
            isExpenditureOpen, category,
            consumption
        },
    } = useContext(UserContext);
    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

    return (
            <CentralBlockWrapper>
                <ItemWrapper>
                    <Item background={COLORS[0]}>
                    </Item>
                    <ItemLabel>
                        Conductive goods
                    </ItemLabel>
                </ItemWrapper>
                <ItemWrapper>
                    <Item background={COLORS[1]}>
                    </Item>
                    <ItemLabel>
                        Utility bills
                    </ItemLabel>
                </ItemWrapper>
                <ItemWrapper>
                    <Item background={COLORS[2]}>
                    </Item>
                    <ItemLabel>
                        Recreation
                    </ItemLabel>
                </ItemWrapper>
                <ItemWrapper>
                    <Item background={COLORS[3]}>
                    </Item>
                    <ItemLabel>
                        Services
                    </ItemLabel>
                </ItemWrapper>
                <Footer>
                    <UpdateButton>Update budget</UpdateButton>
                    <Label>
                        Current budget 0$
                    </Label>
                    <ProgressBarWrapper>
                        <ProgressBar now={0} label={'0%'}/>
                    </ProgressBarWrapper>
                </Footer>
            </CentralBlockWrapper>
    )
}

export default CentralBlock;