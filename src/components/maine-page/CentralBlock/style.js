import styled from "styled-components";

export const CentralBlockWrapper = styled.div`
  display: flex;
  height: 100%;
  width: calc(100% / 3);
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-evenly;
`;

export const Footer = styled.div`
  height: 20%;
  width: 100%;
`;

export const ProgressBarWrapper = styled.div`
  height: 80%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

export const ItemWrapper = styled.div`
  display: flex;
  height: 10%;
  width: 100%;
  align-items: flex-end;
`;

export const Item = styled.div`
  height: 30px;
  width: 30px;
  background-color: ${props => props.background};
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 10%;
  color: white;
  user-select: none;
  cursor: pointer;
  border: 1px solid black;
  box-shadow: 2px 2px 2px 2px black;
`;

export const ItemLabel = styled.div`
  display: flex;
  color: #fff;
  height: 100%;
  width: 60%;
  justify-content: flex-start;
  align-items: center;
  flex-direction: row;
  margin-left: 10%;
`;

export const UpdateButton = styled.div`
  height: 20px;
  width: 150px;
  background-color: #ff9900;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 2%;
  color: white;
  user-select: none;
  cursor: pointer;
  border: 1px solid black;
  box-shadow: 2px 2px 2px 2px black;
`;

export const Label = styled.div`
  margin-top: 10px;
  margin-bottom: 10px;
  display: flex;
  color: #fff;
  height: 15px;
  width: 100%;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;