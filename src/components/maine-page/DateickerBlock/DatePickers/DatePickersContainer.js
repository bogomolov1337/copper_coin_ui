import {useSelector} from 'react-redux';
import React, {useContext} from 'react';
import DatePickers from './DatePickers';
import {UserContext} from "../../../userContext/UserContext";
import * as selectors from "../../../../redux/selectors/selectors";
import {DatePickerInnerWrapper, EmptyBlock} from "../style";

const DatePickersContainer = () => {
    const {
        actions: {
            setStartDate,
            setEndDate,
        },
        selectors: {},
    } = useContext(UserContext);

    let endDate = useSelector(selectors.getDatePickerEndDate);
    endDate = (!endDate || endDate.length === 0) ? null : new Date(endDate);

    let startDate = useSelector(selectors.getDatePickerStartDate);
    startDate = (!startDate || startDate.length === 0) ? null : new Date(startDate);

    const endDatePlaceholder = (startDate && !endDate) ? new Date() : endDate;

    const handleChangeEndDate = (newValue) => {
        setEndDate(newValue);
    };

    const checkEndData = () => {
        if (endDate) {
            setEndDate(endDate);
        } else {
            setEndDate(new Date());
        }
    };

    const handleChangeStartDate = (newValue) => {
        setStartDate(newValue);
        checkEndData();
    };

    const getSelectedDate = (date) => {
        const newDate = new Date(date);
        const day = newDate.getDate();
        const year = newDate.getFullYear();
        const month = newDate.getMonth();

        return new Date().setFullYear(year, month, day);
    };

    const maxDate = endDate ? getSelectedDate(endDate) : new Date().setDate(new Date().getDate());
    const maxEndDate = new Date().setDate(new Date().getDate());
    return (
        <DatePickerInnerWrapper>
            <DatePickers dateState={startDate} maxDate={maxDate} handleChange={handleChangeStartDate}/>
            <EmptyBlock />
            <DatePickers dateState={endDatePlaceholder} maxDate={maxEndDate} minData={startDate}
                         handleChange={handleChangeEndDate}/>
        </DatePickerInnerWrapper>
    );
};

export default DatePickersContainer;
