import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import {
  StyledButton,
  DatePickerWrapper, CalendarIcon,
} from './style';
import { useTranslation } from 'react-i18next';

const DatePickers = (props) => {
  const { t } = useTranslation();
  const datePickerPattern = 'dd/MM/yyyy';

  const {
    dateState, handleChange, minData, maxDate, dateFormat
  } = props;
  const StyledInput = React.forwardRef(({ value, onClick }, ref) => (
    <StyledButton onClick={onClick} ref={ref}>
      {value || '--/--/----'}
      <CalendarIcon />
    </StyledButton>
  ));

  return (
    <DatePickerWrapper title={t('filters.data_pickers_title')}>
      <DatePicker
        dateFormat={dateFormat}
        selected={dateState}
        customInput={<StyledInput />}
        onChange={(date) => handleChange(date)}
        maxDate={maxDate}
        minDate={minData}
      />

    </DatePickerWrapper>
  );
};

export default DatePickers;
