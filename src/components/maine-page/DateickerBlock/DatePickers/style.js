import styled from 'styled-components';

export const StyledButton = styled.button`
  color: black;
  background-color: ghostwhite;
  border: 1px solid;
  border-radius: 0.3rem;
  width: 100px;
  height: 36px;
  display: flex;
  -webkit-align-items: flex-end;
  -webkit-box-align: end;
  -ms-flex-align: end;
  margin-top: auto;
  justify-content: flex-end;
  align-items: center;
  padding: 0 0 0 25px;

  &:hover {
    transition: all .2s cubic-bezier(.47, 0, .745, .715);
    color: #0c202e;
    border-color: #ff9900;
  }
`;

export const DatePickerWrapper = styled.div`
  width: fit-content;
`;
export const CalendarIcon = styled.div`
  height: 24px;
  width: 24px;
  padding: 0 4px;
  border: none;
  outline: none !important;
  display: flex;
  flex-direction: column;
  text-align: center;
  vertical-align: middle;

  background-repeat: no-repeat;
  background-position: center;
  overflow: hidden;
  vertical-align: middle;
`;
