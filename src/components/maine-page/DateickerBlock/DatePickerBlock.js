import {DatePickerWrapper, ShowExpensesBtn} from "./style";
import React from "react";
import DatePickersContainer from "./DatePickers/DatePickersContainer";

const DatePickerBlock = () => {

    return (
        <DatePickerWrapper>
            <DatePickersContainer/>
            <ShowExpensesBtn>Show expenses</ShowExpensesBtn>
        </DatePickerWrapper>
    )
}

export default DatePickerBlock;