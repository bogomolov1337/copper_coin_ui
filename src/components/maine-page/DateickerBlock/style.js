import styled from "styled-components";

export const DatePickerWrapper = styled.div`
  display: flex;
  height: 100%;
  width: calc(100% / 3);
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
`;

export const DatePickerInnerWrapper = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  justify-content: flex-start;
`;

export const EmptyBlock = styled.div`

  width: 15%;

`;

export const ShowExpensesBtn = styled.button`
  color: #fff;
  width: 70%;
  margin-top: 20%;
  margin-bottom: 30%;
  height: 10%;
  background-color: #ff9900;
  border: 1px solid black;
  cursor: pointer;
  box-shadow: 2px 2px 2px 2px black;
  margin-right: auto;
`;
