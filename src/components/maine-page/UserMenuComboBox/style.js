import styled from "styled-components";
import Select from 'react-select';
const padding = 10;


export const DropdownWrapper = styled.form`

  display: flex;
  flex-flow: column;
  flex-direction: column;
  justify-content: center
  margin: 30px 35px 0 0;
  width: 50px;
  height: 50px;
  border-radius: 100px;
  text-align: center;
  background-color: #142836;
  border: 1px solid #FF9900;
  margin-right: 15px;
  &:hover { 
   
  }
`;

export const UserMenu = styled.div`
    top: 10%;
    right: -55px;
    position: absolute;
    width: 140px;
    height: 100px;
    margin-right: 60px;
    border-radius: 15px;
    border: 1px solid #FF9900;
    background-color: #142836;
    outline: 0 !important;
    display: flex;
    justify-content: flex-start
    align-content: flex-start
    align-items: center;
    flex-direction: column;
    align-items: center;
    color: #FF9900;

`;

export const UserMenuButton = styled.div`
      width: 100%;
      height: 50%;
      cursor:pointer;
      display: flex;
      justify-content: center;
      align-items: center;
      border-top: ${props => props.border}

`;

export const StyledSelect = styled.select`
  max-width: 50%;
  height: 100%;
  padding: 0.5rem;
  margin-bottom: 1rem;
`;

export const StyledButton = styled.input`
  max-width: 50%;
  height: 100%;
  display: flex;
  justify-content: center;
  border: solid 2px blue;
  padding: 0.5rem;
  border-radius: 1rem;
`;

export const StyledLabel = styled.label`
  margin-bottom: 1rem;
  cursor: pointer;
  font-size: 40px;
  color: #ff9900;
  user-select: none;
    height: 100%;
    display: flex;
    align-content: center;
    justify-content: center;
    flex-direction: row;
    align-items: flex-start;
    flex-wrap: wrap;
`;

export const StyledComboBoxSelect = styled(Select)`
   width: 19.1em;
`;

export const FilterPanelCompany = styled.div`
    display: grid;
    grid-template-columns: auto ;
    padding: ${padding}px;
    z-index: 4;

    div {
        cursor:pointer;
    }
`;
