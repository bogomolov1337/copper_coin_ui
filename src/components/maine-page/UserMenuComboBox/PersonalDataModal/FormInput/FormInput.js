import React from 'react';
import {ErrorMsg, InputWrapper, Label, Wrapper} from './style';

const FormInput = (props) => {
    const {
        max,
        min,
        labelText,
        inputType,
        name,
        handleChange,
        styleValidation,
        value,
        handleBlur,
        errorData,
        id,
    } = props;
    return (
        <Wrapper data-at="form-input_wrapper" >
            <Wrapper.Label data-at="wrapper_label">
                <Label.Text
                    data-at="label_text"
                    children={labelText}
                />
            </Wrapper.Label>
            <InputWrapper>
                <Wrapper.Input
                    type={inputType}
                    name={name}
                    data-at="wrapper_input"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    borderColor={styleValidation}
                    maxLength={max}
                    minLength={min}
                    value={value}
                    id={id}
                />
               <ErrorMsg children={errorData ? errorData : ''} />
            </InputWrapper>
        </Wrapper>
    );
};

export default FormInput;
