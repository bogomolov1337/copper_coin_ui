import styled from "styled-components";
import {CloseButton} from "../../../registration/ModalWindow/style";

export const LabelText = styled.div`
  display: contents;
  margin-left: 5%;
  margin-right: auto;
  font-family: "Roboto";
  color: #fff;
  font-size: 16px;
  font-weight: normal;
  user-select: none;
`;

export const ModalWindow = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 4;
`;

export const Blocker = styled.div`
  position: fixed;
  background: rgba(0, 0, 0, .7);
  width: 100%;
  height: 100%;
`;

export const ModalWindowBody = styled.div`
  width: 480px;
  height: 600px;
  background: #0c202e;
  border-radius: 4px;
  z-index: 1;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const Header = styled.div`
  width: 100%;
  height: 10%;
  padding: 18px 24px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  user-select: none;
`;

export const Body = styled.div`
  width: 100%;
  height: 80%;
  display: flex;
  justify-content: space-evenly;
  flex-direction: column;
  align-items: center;
  user-select: none;
`;

export const Footer = styled.div`
  height: 10%;
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  margin-bottom: 10px;
`;

export const SubmitButton = styled.button`
  width: 80px;
  height: 50px;
  border: 1px solid;
  box-sizing: border-box;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 16px;
  color: #fff;
  background: #142836;
  cursor: pointer;

  &:hover {
    transition: all .2s cubic-bezier(.47, 0, .745, .715);
    background: #ff9900;
    color: #142836;
  }
`;


ModalWindowBody.Footer = Footer;

ModalWindowBody.Header = Header;

ModalWindowBody.Header.CloseButton = CloseButton;

ModalWindow.Blocker = Blocker;
