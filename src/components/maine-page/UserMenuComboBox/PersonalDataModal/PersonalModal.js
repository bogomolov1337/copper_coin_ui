import React, {useContext, useEffect} from 'react';
import {UserContext} from "../../../userContext/UserContext";
import {Body, LabelText, ModalWindow, ModalWindowBody, SubmitButton} from "./style";
import FormInput from "./FormInput/FormInput";
import {Formik} from 'formik';
import * as yup from "yup";

const PersonalDataModal = () => {

    const {
        actions: {onPersonalModalChange},
        selectors: {},
    } = useContext(UserContext);

    useEffect(() => {
        const close = (e) => {
            if (e.keyCode === 27) {
                onPersonalModalChange();
            }
        };
        window.addEventListener('keydown', close);
        return () => window.removeEventListener('keydown', close);
    }, []);

    return (
        <ModalWindow>
            <ModalWindow.Blocker/>
            <ModalWindowBody>
                <ModalWindowBody.Header>
                    <LabelText>Personal data</LabelText>
                    <ModalWindowBody.Header.CloseButton onClick={onPersonalModalChange} title="Close">
                        <CloseIcon
                            width="14"
                            height="14"
                            viewBox="0 0 14 14"
                        />
                    </ModalWindowBody.Header.CloseButton>
                </ModalWindowBody.Header>
                <Formik
                    initialValues={{
                        firstName: '',
                        lastName: '',
                        login: '',
                        email: '',
                        phone: '',
                        password: '',
                        newPassword: '',
                        confirmPassword: '',
                    }}
                    validateOnBlur
                    onSubmit={(values) => console.log(values)}
                    validationSchema={yup.object().shape({
                        firstName: yup.string()
                            .min(4, "*Min length 4 symbols")
                            .max(64, "*Max length 64 symbols"),
                        lastName: yup.string()
                            .min(4, "*Min length 4 symbols")
                            .max(64, "*Max length 64 symbols"),
                        login: yup.string()
                            .min(4, "*Min length 4 symbols")
                            .max(64, "*Max length 64 symbols"),
                        email: yup.string().email("*Invalid email format").typeError(),
                        phone:  yup.string().matches(/(?=.*[0-9]{10, 12})/, 'Only digits'),
                        password: yup.string().typeError('validation.required_field')
                            .matches(/(?=.*[0-9])/, '*Must be one number')
                            .matches(/(?=.*[a-z])/, '*Must be one small symbol ')
                            .matches(/(?=.*[A-Z])/, '*Must be one uppercase symbol')
                            .min(4, '*Min length 4 symbols')
                            .required('*Required field'),
                        newPassword: yup.string()
                            .matches(/(?=.*[0-9])/, '*Must be one number')
                            .matches(/(?=.*[a-z])/, '*Must be one small symbol ')
                            .matches(/(?=.*[A-Z])/, '*Must be one uppercase symbol')
                            .min(4, '*Min length 4 symbols'),
                        confirmPassword: yup.string()
                            .matches(/(?=.*[0-9])/, '*Must be one number')
                            .matches(/(?=.*[a-z])/, '*Must be one small symbol ')
                            .matches(/(?=.*[A-Z])/, '*Must be one uppercase symbol')
                            .min(4, '*Min length 4 symbols').oneOf([yup.ref('newPassword'), null], '*Passwords must match'),
                    })}
                >
                    {({
                          values, errors, touched, handleChange, handleBlur, isValid, handleSubmit, dirty,
                      }) => (
                        <>
                            <Body>

                                <FormInput
                                    id={'personal_data.first_name_input'}
                                    max={64}
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    labelText={'First name'}
                                    name="firstName"
                                    value={values.firstName}
                                    errorData={touched.firstName && errors.firstName}
                                    styleValidation={touched.firstName && errors.firstName ? "red": "#fff"}
                                />

                                <FormInput
                                    id={'personal_data.last_name_input'}
                                    max={64}
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    name="lastName"
                                    labelText={'Last name'}
                                    value={values.lastName}
                                    errorData={touched.lastName && errors.lastName}
                                    styleValidation={touched.lastName && errors.lastName ? "red": "#fff"}
                                />

                                <FormInput
                                    id={'personal_data.login_input'}
                                    max={40}
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    labelText={'Login'}
                                    name="login"
                                    value={values.login}
                                    errorData={touched.login && errors.login}
                                    styleValidation={touched.login && errors.login ? "red": "#fff"}
                                />

                                <FormInput
                                    id={'personal_data.email_input'}
                                    max={40}
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    labelText={'Email'}
                                    name="email"
                                    value={values.email}
                                    errorData={touched.email && errors.email}
                                    styleValidation={touched.email && errors.email ? "red": "#fff"}
                                />

                                <FormInput
                                    id={'personal_data.phone_input'}
                                    max={12}
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    labelText={'Phone'}
                                    name="phone"
                                    value={values.phone}
                                    errorData={touched.phone && errors.phone}
                                    styleValidation={touched.phone && errors.phone ? "red": "#fff"}
                                />

                                <FormInput
                                    id={'personal_data.password_input'}
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    labelText={'Password'}
                                    inputType="password"
                                    name="password"
                                    value={values.password}
                                    errorData={touched.password && errors.password}
                                    styleValidation={touched.password && errors.password ? "red": "#fff"}
                                />

                                <FormInput
                                    id={'personal_data.new_password_input'}
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    labelText={'New password'}
                                    inputType="password"
                                    name="newPassword"
                                    value={values.newPassword}
                                    errorData={touched.newPassword && errors.newPassword}
                                    styleValidation={touched.newPassword && errors.newPassword ? "red": "#fff"}
                                />

                                <FormInput
                                    id={'personal_data.confirm_password_input'}
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    labelText={'Confirm password'}
                                    inputType="password"
                                    name="confirmPassword"
                                    value={values.confirmPassword}
                                    errorData={touched.confirmPassword && errors.confirmPassword}
                                    styleValidation={touched.confirmPassword && errors.confirmPassword ? "red": "#fff"}
                                />

                            </Body>
                            <ModalWindowBody.Footer>
                                <SubmitButton
                                    disabled={!isValid && !dirty}
                                    onClick={handleSubmit}
                                    type="submit"
                                    children={"Submit"}
                                />
                            </ModalWindowBody.Footer>
                        </>
                    )}
                < /Formik>


            </ModalWindowBody>
        </ModalWindow>
    );
};

export default PersonalDataModal;

export function CloseIcon(props) {
    return (
        <svg {...props}>
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M7.00008 8.41429L12.293 13.7072L13.7072 12.293L8.41429 7.00008L13.7072 1.70718L12.293 0.292969L7.00008
         5.58586L1.70718 0.292969L0.292969 1.70718L5.58586 7.00008L0.292969 12.293L1.70718 13.7072L7.00008 8.41429Z"
                fill="#79838A"
            />
        </svg>
    );
}