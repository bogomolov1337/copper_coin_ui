import React, {useContext} from 'react';
import {OkButton} from './style';
import {UserContext} from "../../../../userContext/UserContext";

const OkButtonContainer = (props) => {
    const {
        actions: { onPersonalModalChange,  },
        selectors: {
        },
    } = useContext(UserContext);
    const { handleClick } = props;

    return (
            <OkButton onClick={handleClick}>Confirm</OkButton>
    );
};

export default OkButtonContainer;
