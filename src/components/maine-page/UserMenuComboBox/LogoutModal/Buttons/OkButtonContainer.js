import React, {useContext} from 'react';
import {OkButton} from './style';
import {NavLink} from "react-router-dom";
import {UserContext} from "../../../../userContext/UserContext";

const OkButtonContainer = (props) => {
    const {
        actions: { onLogoutModalChange, logoutUser },
        selectors: {
        },
    } = useContext(UserContext);

    const handleClick = () => {
        logoutUser();
        onLogoutModalChange();
    };

    return (
        <NavLink to='/login'>
            <OkButton onClick={handleClick}>Yes</OkButton>
        </NavLink>
    );
};

export default OkButtonContainer;
