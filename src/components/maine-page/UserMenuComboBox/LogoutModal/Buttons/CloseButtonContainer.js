import React, {useContext} from 'react';
import {OkButton} from './style';
import {NavLink} from "react-router-dom";
import {UserContext} from "../../../../userContext/UserContext";

const OkButtonContainer = (props) => {
    const {
        actions: { onLogoutModalChange },
        selectors: {
        },
    } = useContext(UserContext);

    const handleClick = () => {
        onLogoutModalChange();
    };

    return (
            <OkButton onClick={handleClick}>No</OkButton>
    );
};

export default OkButtonContainer;
