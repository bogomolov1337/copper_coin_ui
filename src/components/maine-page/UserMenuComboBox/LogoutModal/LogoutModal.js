import React, {useContext, useEffect} from 'react';
import {UserContext} from "../../../userContext/UserContext";
import {LabelText, ModalWindow, ModalWindowBody} from "./style";
import OkButtonContainer from "./Buttons/OkButtonContainer";
import CloseButtonContainer from "./Buttons/CloseButtonContainer";


const LogoutModal = () => {

    const {
        actions: {onLogoutModalChange},
        selectors: {
        },
    } = useContext(UserContext);

    useEffect(() => {
        const close = (e) => {
            if (e.keyCode === 27) {
                onLogoutModalChange();
            }
        };
        window.addEventListener('keydown', close);
        return () => window.removeEventListener('keydown', close);
    }, []);

    return (
        <ModalWindow>
            <ModalWindow.Blocker/>
            <ModalWindowBody>
                <ModalWindowBody.Header>
                    <ModalWindowBody.Header.CloseButton onClick={onLogoutModalChange} title="Close">
                        <CloseIcon
                            width="14"
                            height="14"
                            viewBox="0 0 14 14"
                        />
                    </ModalWindowBody.Header.CloseButton>
                </ModalWindowBody.Header>
                <LabelText>Do you want to log out?</LabelText>
                <ModalWindowBody.Footer>
                    <OkButtonContainer/>
                    <CloseButtonContainer/>
                </ModalWindowBody.Footer>


            </ModalWindowBody>
        </ModalWindow>
    );
};

export default LogoutModal;

export function CloseIcon(props) {
    return (
        <svg {...props}>
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M7.00008 8.41429L12.293 13.7072L13.7072 12.293L8.41429 7.00008L13.7072 1.70718L12.293 0.292969L7.00008
         5.58586L1.70718 0.292969L0.292969 1.70718L5.58586 7.00008L0.292969 12.293L1.70718 13.7072L7.00008 8.41429Z"
                fill="#79838A"
            />
        </svg>
    );
}