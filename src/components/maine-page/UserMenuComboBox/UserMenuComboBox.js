import React, {useContext, useState} from 'react';
import {useTranslation} from 'react-i18next';
import {UserContext} from '../../userContext/UserContext';
import {DropdownWrapper, LogoutButton, StyledLabel, UserMenu, UserMenuButton} from "./style";
import {useDispatch} from "react-redux";
import actions from "../../../redux/actions";
import LogoutModal from "./LogoutModal/LogoutModal";
import PersonalModal from "./PersonalDataModal/PersonalModal";

const UserMenuComboBox = () => {
    const {
        actions: {onLogoutModalChange, onPersonalModalChange},
        selectors: {user, isLogoutOpen, isPersonalDataOpen},
    } = useContext(UserContext);
    const {t} = useTranslation();
    const dispatch = useDispatch();
    const [showCombo, setShowCombo] = useState(false);

    // const filterData = convertCompanyDtoForFilter('companies');

    const handleChange = (newValue) => {
    };

    const onMouseEnter = () => {
        setShowCombo(!showCombo)
    }

    const onLogout = () => {
        onLogoutModalChange()
    }

    const onPersonData = () => {
        onPersonalModalChange()
    }

    return (
        <DropdownWrapper>
            <StyledLabel onClick={onMouseEnter}>{user.FIRST_NAME ? user.FIRST_NAME[0] : 'N'}</StyledLabel>
            {isLogoutOpen && <LogoutModal />}
            {isPersonalDataOpen && <PersonalModal />}
            {showCombo && <UserMenu>
                <UserMenuButton onClick={onPersonData} >Personal data</UserMenuButton>
                <UserMenuButton border={" 1px solid #ff9900"} onClick={onLogout}>Log out</UserMenuButton>
            </UserMenu>}
        </DropdownWrapper>
    );
};

export default UserMenuComboBox;
