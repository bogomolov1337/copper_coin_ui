import {Body, Header, ListOfExpensesWrapper} from "./style";
import React, {useContext} from "react";

const ListOfExpenses = () => {

    return (
        <ListOfExpensesWrapper>
            <Header>List of expenses</Header>
            <Body>

            </Body>
        </ListOfExpensesWrapper>
    )
}

export default ListOfExpenses;