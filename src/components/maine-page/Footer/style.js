import styled from "styled-components";

export const ListOfExpensesWrapper = styled.div`
    height: 100%;
    width: 60%;
    margin-left: 2%;
`;

export const StatisticOfExpensesWrapper = styled.div`
    height: 100%;
    width: 40%;
    margin-left: 5%;
`;

export const Header = styled.div`
    height: 20%;
    width: 100%;
    margin-left: 5%;
    color: #fff;
    font-family: "Roboto";
`;
export const Body = styled.div`
    height: 80%;
    width: 100%;
    margin-left: 5%;
    border: 1px solid #ff9900;
`;
export const StatisticBody = styled.div`
    height: 80%;
    width: 90%;
    margin-left: 5%;
    border: 1px solid #ff9900;
`;