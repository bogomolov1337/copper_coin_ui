import {Header, StatisticBody, StatisticOfExpensesWrapper} from "./style";
import React from "react";

const StatisticOfExpenses = () => {

    return (
        <StatisticOfExpensesWrapper>
            <Header>Statistic of expenses</Header>
            <StatisticBody>

            </StatisticBody>
        </StatisticOfExpensesWrapper>
    )
}

export default StatisticOfExpenses