import styled from "styled-components";

export const DiagramWrapper = styled.div`
  display: flex;
  height: 100%;
  width: calc(100% / 3);
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-end;
`;

export const Header = styled.header`
  font-family: "Roboto";
  width: 100%;
  color: #fff;
  margin-top: 10%;
  margin-left: 10%;
`;

export const Diagram = styled.header`
  height: 40%;
  width: 80%;
  margin-bottom: auto;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  margin-left: 10%;
  width: 100%;
  height: 35%;
  flex-direction: column;
`;

export const Label = styled.div`
  color: #fff;
  font-size: 16px;
  display: flex;
  margin-top: 3%;
  margin-left: 10%;
`;

export const EmptyDiagramText = styled.div`
  color: black;
  font-size: 12px;
  width: 150px;
  position: absolute;
  top: 23%;
  margin-top: 4%;
  z-index: 1;
  margin-left: 10%;
  left: -2%;
  user-select: none;
`;

export const ButtonWrapperInner = styled.div`
  display: flex;
  width: 50%;
  height: 50%;
`;

export const Button = styled.div`
  height: 50px;
  width: 50px;
  background-image: url(${(props) => props.imageUrl});
  background-repeat: no-repeat;
  background-position: center;
  cursor: pointer;
  border-radius: 100px;
  background-color: #142836;
  margin-bottom: 20%;
  border: 1px solid #FF9900;
`;

