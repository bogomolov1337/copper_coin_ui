import styled from "styled-components";
import {CloseButton} from "../../../registration/ModalWindow/style";
import Dropdown from "react-dropdown";

const inputsFontSize = 20;
const inputBorderWidth = 1;
const inputBorderRadius = 4;

export const LabelText = styled.div`
  display: contents;
  margin-left: 5%;
  margin-right: auto;
  font-family: "Roboto";
  color: #fff;
  font-size: 16px;
  font-weight: normal;
  user-select: none;
`;

export const ModalWindow = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 4;
`;

export const Blocker = styled.div`
  position: fixed;
  background: rgba(0, 0, 0, .7);
  width: 100%;
  height: 100%;
`;

export const ModalWindowBody = styled.div`
  width: 600px;
  height: 400px;
  background: #0c202e;
  border-radius: 4px;
  z-index: 1;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const Header = styled.div`
  width: 100%;
  height: 10%;
  padding: 18px 24px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  user-select: none;
`;

export const Body = styled.div`
  width: 100%;
  height: 80%;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  align-items: flex-start;
  user-select: none;
`;

export const Footer = styled.div`
  height: 10%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 10px;
`;

export const SubmitButton = styled.button`
  width: 80px;
  height: 50px;
  border: 1px solid;
  box-sizing: border-box;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 16px;
  color: #fff;
  background: #142836;
  cursor: pointer;
  margin-left: 10%;
  margin-bottom: 5%;
  
  &:hover {
    transition: all .2s cubic-bezier(.47, 0, .745, .715);
    background: #ff9900;
    color: #142836;
  }
`;

export const AddCategoryButton = styled.button`
  width: 160px;
  height: 24px;
  border: 1px solid;
  box-sizing: border-box;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 16px;
  color: #fff;
  background: #142836;
  cursor: pointer;
  margin-left: 5%;
  
  &:hover {
    transition: all .2s cubic-bezier(.47, 0, .745, .715);
    background: #ff9900;
    color: #142836;
  }
`;

export const Input = styled.input`
  width: 25%;
  height: 24px;
  margin-left: 5%;
  outline: none;
  border-radius: ${inputBorderRadius}px;
  font-size: ${inputsFontSize}px;
  border: ${inputBorderWidth}px;
  color: #D3DBE0;
  background-color: #263845;
  font-family: 'Roboto', sans-serif;
  border: 1px solid ${(props) => props.borderColor};
  margin-right: 5%;

  :-webkit-autofill,
  :-webkit-autofill:hover,
  :-webkit-autofill:focus {
    -webkit-text-fill-color: #808000;
    -webkit-box-shadow: 0 0 0px 1000px #FDF5E6 inset;
    transition: background-color 5000s ease-in-out 0s;
  }

  &::-ms-clear, &::-ms-reveal {
    display: none;
  }
`;

export const Text = styled.div`
  height: 24px;
  font-family: "Roboto";
  color: #fff;
  font-size: 16px;
  font-weight: normal;
  user-select: none;
`;

export const WrapperInput = styled.div`
  margin-top: 5%;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

export const WrapperInputS = styled.div`
  margin-top: 5%;
  margin-left: 5%;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export const DropdownS = styled.select`
  width: 25%;
  height: 24px;
  margin-left: 5%;
  outline: none;
  border-radius: ${inputBorderRadius}px;
  font-size: 14px;
  border: ${inputBorderWidth}px;
  color: #D3DBE0;
  background-color: #263845;
  font-family: 'Roboto', sans-serif;
  border: 1px solid ${(props) => props.borderColor};
  margin-right: 5%;
  
  :-webkit-autofill,
  :-webkit-autofill:hover,
  :-webkit-autofill:focus {
    -webkit-text-fill-color: #808000;
    -webkit-box-shadow: 0 0 0px 1000px #FDF5E6 inset;
    transition: background-color 5000s ease-in-out 0s;
  }

  &::-ms-clear, &::-ms-reveal {
    display: none;
  }
`;

export const ErrorMsg = styled.div`
  bottom: 7px;
  height: 20px;
  color: #f70303e6;
  width: 100%;
  display: flex;
`;

export const Checkbox = styled.input`
  margin-right: 15px;
`;


ModalWindowBody.Footer = Footer;

ModalWindowBody.Header = Header;

ModalWindowBody.Header.CloseButton = CloseButton;

ModalWindow.Blocker = Blocker;
