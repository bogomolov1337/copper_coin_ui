import React, {useContext, useEffect} from 'react';
import {UserContext} from "../../../userContext/UserContext";
import {
    AddCategoryButton,
    Body,
    Checkbox,
    DropdownS,
    Input,
    LabelText,
    ModalWindow,
    ModalWindowBody,
    SubmitButton,
    Text,
    WrapperInput,
    WrapperInputS
} from "./style";
import {Formik} from 'formik';
import * as yup from "yup";
import AddCategory from "./AddCategory/AddCategory";

const PersonalDataModal = () => {

    const {
        actions: {setExpenditureWindowState, setAddCategoryWindowState, createExpenses},
        selectors: {isAddCategoryOpen, category, user},
    } = useContext(UserContext);

    useEffect(() => {
        const close = (e) => {
            if (e.keyCode === 27) {
                setExpenditureWindowState();
            }
        };
        window.addEventListener('keydown', close);
        return () => window.removeEventListener('keydown', close);
    }, []);

    const options = Object.values(category).map(item => item.alias)

    const onSubmit = (values) => {
        const payload = {
            userId: user.sub,
            arg: `{\"amount\":${values.amount},\"category\":\"${values.category}\",\"whenCreated\":[2021,10,31,8,48,29,416237200],\"forThePeriod\":[2021,10,31,8,48,29,417262500],\"require\":${values.require}}`,
            cmd: "ADD_CONSUMPTION"
        }
        createExpenses(payload)
    }
    return (
        <ModalWindow>
            <ModalWindow.Blocker/>
            <ModalWindowBody>
                <ModalWindowBody.Header>
                    <LabelText>Add expenditure</LabelText>
                    <ModalWindowBody.Header.CloseButton onClick={setExpenditureWindowState} title="Close">
                        <CloseIcon
                            width="14"
                            height="14"
                            viewBox="0 0 14 14"
                        />
                    </ModalWindowBody.Header.CloseButton>
                </ModalWindowBody.Header>
                <Formik
                    initialValues={{
                        title: '',
                        amount: '',
                        category: '',
                        require: 'false',
                    }}
                    validateOnBlur
                    onSubmit={(values) => onSubmit(values)}
                    validationSchema={yup.object().shape({})}
                >
                    {({
                          values, errors, touched, handleChange, handleBlur, isValid, handleSubmit, dirty,
                      }) => (
                        <>
                            <Body>
                                <WrapperInput>
                                    <Text children={"Title"}/>
                                    <Input
                                        name="title"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.title}
                                    />
                                    <Text children={"Expenditure"}/>
                                    <Input
                                        name="amount"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.amount}
                                    />
                                </WrapperInput>
                                <WrapperInputS>
                                    <Text children={"Date"}/>
                                    <Input
                                        name="date"
                                    />
                                    <Checkbox type={'checkbox'}
                                              name="require"
                                              onBlur={handleBlur}
                                              onChange={handleChange}
                                              value={values.require}/>
                                    <Text children={"Obligatory expense"}/>
                                </WrapperInputS>
                                <WrapperInputS>
                                    <Text children={"Category"}/>
                                    <DropdownS name="category"
                                               onBlur={handleBlur}
                                               onChange={handleChange}
                                               options={options} value={values.category} placeholder="Select category"
                                               >
                                        {options.map(option => <option key={option} value={touched? option : 'Select category'} label={touched? option : 'Select category'}/>)}
                                    </DropdownS>
                                    {isAddCategoryOpen && <AddCategory/>}
                                    <AddCategoryButton onClick={setAddCategoryWindowState} children={"Add category"}/>
                                </WrapperInputS>
                            </Body>
                            <ModalWindowBody.Footer>
                                <SubmitButton
                                    disabled={!isValid && !dirty}
                                    onClick={handleSubmit}
                                    type="submit"
                                    children={"Add"}
                                />
                            </ModalWindowBody.Footer>
                        </>
                    )}
                < /Formik>
            </ModalWindowBody>
        </ModalWindow>
    );
};

export default PersonalDataModal;

export function CloseIcon(props) {
    return (
        <svg {...props}>
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M7.00008 8.41429L12.293 13.7072L13.7072 12.293L8.41429 7.00008L13.7072 1.70718L12.293 0.292969L7.00008
         5.58586L1.70718 0.292969L0.292969 1.70718L5.58586 7.00008L0.292969 12.293L1.70718 13.7072L7.00008 8.41429Z"
                fill="#79838A"
            />
        </svg>
    );
}