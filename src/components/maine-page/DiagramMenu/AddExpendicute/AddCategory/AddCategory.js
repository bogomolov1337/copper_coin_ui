import React, {useContext, useEffect} from 'react';
import {Formik} from 'formik';
import * as yup from "yup";
import {UserContext} from "../../../../userContext/UserContext";
import {Body, Input, LabelText, ModalWindow, ModalWindowBody, SubmitButton, Text} from "./style";

const PersonalDataModal = () => {

    const {
        actions: {setAddCategoryWindowState, createCategory},
        selectors: {user},
    } = useContext(UserContext);

    useEffect(() => {
        const close = (e) => {
            if (e.keyCode === 27) {
                setAddCategoryWindowState();
            }
        };
        window.addEventListener('keydown', close);
        return () => window.removeEventListener('keydown', close);
    }, []);

    const onSubmit = (value) => {
        const payload = {userId: user.sub, arg: `{\"alias\":\"${value}\"}`, cmd: "CREATE_CATEGORY"};
        createCategory(payload)
    }

    return (
        <ModalWindow>
            <ModalWindow.Blocker/>
            <ModalWindowBody>
                <ModalWindowBody.Header>
                    <LabelText>Add category</LabelText>
                    <ModalWindowBody.Header.CloseButton onClick={setAddCategoryWindowState} title="Close">
                        <CloseIcon
                            width="14"
                            height="14"
                            viewBox="0 0 14 14"
                        />
                    </ModalWindowBody.Header.CloseButton>
                </ModalWindowBody.Header>
                <Formik
                    initialValues={{
                        category: '',
                    }}
                    validateOnBlur
                    onSubmit={(values) => onSubmit(values.category)}
                    validationSchema={yup.object().shape({
                    })}
                >
                    {({
                          values, errors, touched, handleChange, handleBlur, isValid, handleSubmit, dirty,
                      }) => (
                        <>
                            <Body>
                                <Text children={"Category"}/>
                                <Input
                                    name="category"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values.category}
                                />
                            </Body>
                            <ModalWindowBody.Footer>
                                <SubmitButton
                                    disabled={!isValid && !dirty}
                                    onClick={handleSubmit}
                                    type="submit"
                                    children={"Add"}
                                />
                            </ModalWindowBody.Footer>
                        </>
                    )}
                < /Formik>
            </ModalWindowBody>
        </ModalWindow>
    );
};

export default PersonalDataModal;

export function CloseIcon(props) {
    return (
        <svg {...props}>
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M7.00008 8.41429L12.293 13.7072L13.7072 12.293L8.41429 7.00008L13.7072 1.70718L12.293 0.292969L7.00008
         5.58586L1.70718 0.292969L0.292969 1.70718L5.58586 7.00008L0.292969 12.293L1.70718 13.7072L7.00008 8.41429Z"
                fill="#79838A"
            />
        </svg>
    );
}