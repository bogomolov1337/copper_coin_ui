import {
    Button,
    ButtonWrapper,
    ButtonWrapperInner,
    Diagram,
    DiagramWrapper,
    EmptyDiagramText,
    Header,
    Label
} from "./style";
import React, {useContext} from "react";
import {Cell, Pie, PieChart, ResponsiveContainer} from 'recharts';
import {UserContext} from "../../userContext/UserContext";
import AddExpendicute from "./AddExpendicute/AddExpendicute";

const DiagramBlock = () => {
    const {
        actions: {setExpenditureWindowState},
        selectors: {
            isExpenditureOpen, category,
            consumption
        },
    } = useContext(UserContext);

    const plus = require('../../../icons/plus.svg');
    const trash = require('../../../icons/trash.svg');

    const data = [
        {name: 'Group A', value: 400},
        {name: 'Group B', value: 300},
        {name: 'Group C', value: 300},
        {name: 'Group D', value: 200},
    ];

    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
    const DEFAULT_COLOR = ['#FFF'];

    const RADIAN = Math.PI / 180;

    const renderCustomizedLabel = ({cx, cy, midAngle, innerRadius, outerRadius, percent, index}) => {
        const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);

        return null;

        // {/*</text>            <text x={x} y={y} fill="black" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">*/}
        // {/*    {"Your diagram is empty! \nAdd expenditure"/*{`${(percent * 100).toFixed(0)}%`}*!/*/}
        // {/*</text>*/}
    };
    return (
        <DiagramWrapper>
            <Header>Diagram of expenses({'mm.yyyy'})</Header>
            {consumption.length === 0 ? <EmptyDiagramText children={'Your diagram is empty!\n Add expenditure \n'}/> : null}
            <Diagram>
                <ResponsiveContainer width="100%" height="100%" border="1px solid #ff9900">
                    <PieChart>
                        <Pie
                            data={data}
                            cx="50%"
                            cy="50%"
                            labelLine={false}
                            label={renderCustomizedLabel}
                            outerRadius={80}
                            fill="#8884d8"
                            dataKey="value"
                        >
                            {data.map((entry, index) => (
                                <Cell key={`cell-${index}`} fill={DEFAULT_COLOR[index % DEFAULT_COLOR.length]}/>
                            ))}
                        </Pie>
                    </PieChart>
                </ResponsiveContainer>

            </Diagram>
            <ButtonWrapper>
                <ButtonWrapperInner>
                    {isExpenditureOpen && <AddExpendicute/>}
                    <Button onClick={() => setExpenditureWindowState()} imageUrl={plus}/>
                    <Label>Add expenditure</Label>
                </ButtonWrapperInner>
                <ButtonWrapperInner>
                    <Button imageUrl={trash}/>
                    <Label>Remove expenditure</Label>
                </ButtonWrapperInner>
            </ButtonWrapper>
        </DiagramWrapper>
    )
        ;
};

export default DiagramBlock;