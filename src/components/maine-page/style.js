import styled from "styled-components";

export const Header = styled.div`
    height: 10%;
    width: 100%;
    display: flex;
    justify-content: space-between;
    flex-direction: row;
    align-items: center;
`;

export const PremiumButton = styled.div`
    height: 20px;
    width: 200px;
    background-color: #9C2FD0FF;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-left: auto;
    margin-right: 7%;
    color: white;
    user-select: none;
    cursor: pointer;
    border: 1px solid black;
    box-shadow: 2px 2px 2px 2px black;
`;

