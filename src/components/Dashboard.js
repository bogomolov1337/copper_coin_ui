import React from 'react';

import { Wrapper } from './style';
import MainePage from "./maine-page/MainePage";

const Dashboard = () => (
  <Wrapper>
      <MainePage />
  </Wrapper>
);

export default Dashboard;
