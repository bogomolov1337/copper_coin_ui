import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {UserProvider} from './UserContext';
import {history} from '../../store';
import PrivateRoute from '../private-route/PrivateRoute';
import * as selectors from '../../redux/selectors/selectors';
import * as actions from '../../redux/actionsCreator';
import {
    createCategory,
    createExpenses,
    setAddCategoryWindowState,
    setExpenditureWindowState
} from "../../redux/actionsCreator";
import {getAddCategoryDataModalWindowState, getCategory, getCONSUMPTION} from "../../redux/selectors/selectors";


const ContextContainer = () => {
    const dispatch = useDispatch();
    const user = useSelector(selectors.getUser);
    const errorMessage = useSelector(selectors.getMessagesLogin);
    const isOpenModal2faPinWindow = useSelector(selectors.get2faModalWindowState)
    const isOpenModalRegWindow = useSelector(selectors.getRegModalWindowState)
    const isSuccessForgotModalWindow = useSelector(selectors.getSuccessForgotModalWindowState)
    const messageColor = useSelector(selectors.getMessageColor)
    const borderColor = useSelector(selectors.getBorderColor)
    const regMessage = useSelector(selectors.getRegMessage)
    const isLogoutOpen = useSelector(selectors.getLogoutModalWindowState)
    const isPremiumOpen = useSelector(selectors.getPremiumModalWindowState)
    const isPersonalDataOpen = useSelector(selectors.getPersonalDataModalWindowState)
    const isExpenditureOpen = useSelector(selectors.getExpenditureDataModalWindowState)
    const isAddCategoryOpen = useSelector(selectors.getAddCategoryDataModalWindowState)
    const category = useSelector(selectors.getCategory)
    const consumption = useSelector(selectors.getCONSUMPTION)

    const on2faModalChange = () => {
        dispatch(actions.setErrorMessage(''));
        dispatch(actions.showSmsPinError(''));
        dispatch(actions.changeBorderColor(''));
        dispatch(actions.setGet2faWindowState(!isOpenModal2faPinWindow));
    }

    const onRegistrationModalChange = () => {
        dispatch(actions.setErrorMessage(''));
        dispatch(actions.showSmsPinError(''));
        dispatch(actions.changeBorderColor(''));
        dispatch(actions.setRegWindowState(!isOpenModalRegWindow));
    }

    const onSuccessForgotModalChange = () => {
        dispatch(actions.setErrorMessage(''));
        dispatch(actions.showSmsPinError(''));
        dispatch(actions.changeBorderColor(''));
        dispatch(actions.setSuccessForgotWindowState(!isSuccessForgotModalWindow));
    }

    const onLogoutModalChange = () => {
        dispatch(actions.setErrorMessage(''));
        dispatch(actions.showSmsPinError(''));
        dispatch(actions.changeBorderColor(''));
        dispatch(actions.setLogoutWindowState(!isLogoutOpen));
    }

    const onPremiumModalChange = () => {
        dispatch(actions.setErrorMessage(''));
        dispatch(actions.showSmsPinError(''));
        dispatch(actions.changeBorderColor(''));
        dispatch(actions.setPremiumWindowState(!isPremiumOpen));
    }

    const showMessageError = (payload) => {
        dispatch(actions.setErrorMessage(payload));
        dispatch(actions.changeBorderColor("red"));
    }

    const onPersonalModalChange = () => {
        dispatch(actions.setPersonalDataWindowState(!isPersonalDataOpen))
    }

    const loginUser = (payload) => {
        dispatch(actions.loginUser(payload))
    }

    const resetPassUser = (payload) => {
        dispatch(actions.resetUser(payload))
    }

    const regUser = (payload) => {
        dispatch(actions.regUser(payload))
    }

    const resetRegCode = () => {
        dispatch(actions.setRegMessage(''))
    }

    const logoutUser = () => {
        dispatch(actions.logoutUser())
    }

    const closeAllWindow = () => {
        dispatch(actions.closeAllModal())
    }

    const setStartDate = (payload) => {
        dispatch(actions.setDatePickerStartDate(payload));
    };

    const setEndDate = (payload) => {
        dispatch(actions.setDatePickerEndDate(payload));
    };

    const setExpenditureWindowState = () => {
        dispatch(actions.setExpenditureWindowState(!isExpenditureOpen));
    };

    const setAddCategoryWindowState = () => {
        dispatch(actions.setAddCategoryWindowState(!isAddCategoryOpen));
    };

    const createCategory = (payload) => {
        dispatch(actions.createCategory(payload));
    };

    const createExpenses = (payload) => {
        dispatch(actions.createExpenses(payload));
    };

    return (
        <UserProvider value={{
            actions: {
                on2faModalChange,
                showMessageError,
                loginUser,
                regUser,
                onRegistrationModalChange,
                resetRegCode,
                resetPassUser,
                onSuccessForgotModalChange,
                onLogoutModalChange,
                onPremiumModalChange,
                onPersonalModalChange,
                logoutUser,
                setStartDate,
                setEndDate,
                setExpenditureWindowState,
                setAddCategoryWindowState,
                createCategory,
                createExpenses,
            },
            selectors: {
                user,
                isOpenModalRegWindow,
                isOpenModal2faPinWindow,
                isSuccessForgotModalWindow,
                errorMessage,
                borderColor,
                messageColor,
                regMessage,
                isLogoutOpen,
                isPremiumOpen,
                isPersonalDataOpen,
                isExpenditureOpen,
                isAddCategoryOpen,
                category,
                consumption,
            },
        }}
        >
            <PrivateRoute history={history}/>
        </UserProvider>
    );
};

export default ContextContainer;
