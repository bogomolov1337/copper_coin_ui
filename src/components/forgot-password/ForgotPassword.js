import React, {useContext} from 'react';
import {useTranslation} from 'react-i18next';
import {
    Body,
    Button,
    ButtonContainer,
    ButtonReg,
    ButtonRegistrContainer,
    Company,
    ForgotPasswordLink,
    ItemsContainer,
    RegTitle,
    Text,
    Wrapper
} from './style';
import FormInput from './FormInput/FormInput';
import {UserContext} from '../userContext/UserContext';
import logo from "../../logo.png";
import {NavLink} from "react-router-dom";
import SuccessModalWindow from "./ModalWindow/SuccessModalWindow";
import Modal2FAWindow from "../login/Modal2FaWindow/ModalWindow/Modal2FAWindow";

const ForgotPassword = (props) => {
    const {
        onLoginChange,
        onPasswordChange,
        onForgotSubmit,
        userNameColor,
        passColor,
        onKeyPressHandler,
        passwordErrorMessage,
        loginErrorMessage,
        onConfPasswordChange,
        user,
        passConfColor,
    } = props;
    const {t} = useTranslation();
    const {selectors: {isOpenModal2faPinWindow, isSuccessForgotModalWindow}, actions: {resetPassUser,showMessageError}} = useContext(UserContext);

    const handleClick = () => {
        showMessageError('')
        onForgotSubmit()
    }

    const handleClickReg = () => {
        showMessageError('')
    }
    let payload = {loginOrEmail: user.login, newPass: user.password, authCode: ''};

    return (

        <Wrapper data-at="wrapper">
            <ItemsContainer.Company>
                <Company.Logo src={logo}/>
                <Company.Name>Copper coin - don't waste your money</Company.Name>
                <RegTitle>Don't have account?</RegTitle>
                <ButtonRegistrContainer>
                    <NavLink to='/registration'>
                        <ButtonReg id={'forgot-pass.link_to_registration'} onClick={handleClickReg}>
                            <Button.Label children={'Registration'}/>
                        </ButtonReg>
                    </NavLink>
                </ButtonRegistrContainer>
            </ItemsContainer.Company>
            {isOpenModal2faPinWindow && <Modal2FAWindow fufunctio={resetPassUser}  payload={payload}/>}
            {isSuccessForgotModalWindow && <SuccessModalWindow/>}
            <Wrapper.Body data-at="wrapper body">
                <Text>Create new password.</Text>
                <FormInput
                    id={'forgot-pass.email/login_input'}
                    onChange={onLoginChange}
                    labelText={'Email/Login'}
                    styleValidation={userNameColor}
                    onKeyPressHandler={onKeyPressHandler}
                    message={loginErrorMessage}
                />

                <FormInput
                    id={'forgot-pass.new_password_input'}
                    onChange={onConfPasswordChange}
                    labelText={'New password'}
                    inputType="password"
                    styleValidation={passColor}
                    onKeyPressHandler={onKeyPressHandler}
                    message={passwordErrorMessage}
                />

                <FormInput
                    onChange={onPasswordChange}
                    id={'forgot-pass.confirm_password_input'}
                    labelText={'Confirm password'}
                    inputType="password"
                    styleValidation={passConfColor}
                    onKeyPressHandler={onKeyPressHandler}
                    message={passwordErrorMessage}
                />

                <NavLink to='/login'>
                <ForgotPasswordLink id={'forgot-pass.link_to_auth'}>Did you remember your password?</ForgotPasswordLink>
                </NavLink>

                <Body.ButtonContainer>
                    <ButtonContainer.Button id={'forgot-pass.enter_btn'} onClick={handleClick}>
                        <Button.Label children={t('login.login_button')}/>
                    </ButtonContainer.Button>
                </Body.ButtonContainer>
            </Wrapper.Body>
        </Wrapper>
    );
};

export default ForgotPassword;
