import React, {useContext, useState} from 'react';
import {useDispatch} from 'react-redux';
import * as actions from '../../redux/actionsCreator';
import {UserContext} from "../userContext/UserContext";
import ForgotPassword from "./ForgotPassword";

const ForgotPasswordContainer = () => {
    const [userName, setUserName] = useState('');
    const [pass, setPass] = useState('');
    const [passConf, setPassConf] = useState('');
    const [userNameColor, setUserNameColor] = useState('none');
    const [passColor, setPassColor] = useState('none');
    const [passConfColor, setPassConfColor] = useState('none');
    const dispatch = useDispatch();
    const {actions: {on2faModalChange} , actions: {showMessageError}} = useContext(UserContext);
    const [passwordErrorMessage, setPasswordErrorMessage] = useState('');
    const [loginErrorMessage, setLoginErrorMessage] = useState('');


    const onLoginChange = (e) => {
        setUserName(e.target.value);
        setUserNameColor(e.target.value.trim().length ? 'none' : 'red');
        setLoginErrorMessage(e.target.value.trim().length ? '' : 'Empty email/login field.');
    };

    const onPasswordChange = (e) => {
        setPass(e.target.value);
        setPassColor(e.target.value.trim().length ? 'none' : 'red');
        setPasswordErrorMessage(e.target.value.trim().length ? '' : 'Empty password field.');
    };

    const onConfPasswordChange = (e) => {
        setPassConf(e.target.value);
        setPassConfColor(e.target.value.trim().length ? 'none' : 'red');
        setPasswordErrorMessage(e.target.value.trim().length ? '' : 'Empty password field.');
    };

    const onForgotSubmit = () => {
        if (userName.trim().length && pass.trim().length && pass.trim() === passConf.trim()) {
            dispatch(actions.setUser({loginOrEmail: userName, newPass: pass}))
            setPasswordErrorMessage('');
            setLoginErrorMessage('')
            on2faModalChange()
            return;
        }

        if (pass !== passConf || pass.length !== passConf.length || pass.length < 4 || passConf.length < 4) {
            setPassConfColor('red')
            setPassColor('red')
            setPasswordErrorMessage("Passwords don't match");
        } else {
            setPassConfColor('#fff')
            setPasswordErrorMessage("");
        }

        if (!userName.trim().length) {
            setUserNameColor('red');
            setLoginErrorMessage('Empty email/login field.')
        } else {
            setLoginErrorMessage('')
        }



};

const onKeyPressHandler = (event) => {
    if (event.key === 'Enter') {
        onForgotSubmit();
    }
};

return (
    <ForgotPassword
        user={{login: userName, password: pass}}
        onLoginChange={onLoginChange}
        onPasswordChange={onPasswordChange}
        onForgotSubmit={onForgotSubmit}
        userNameColor={userNameColor}
        passColor={passColor}
        passConfColor={passConfColor}
        passwordErrorMessage={passwordErrorMessage}
        loginErrorMessage={loginErrorMessage}
        onKeyPressHandler={onKeyPressHandler}
        onConfPasswordChange={onConfPasswordChange}
    />);
};

export default ForgotPasswordContainer;
