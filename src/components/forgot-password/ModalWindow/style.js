import styled from 'styled-components';

export const ModalWindow = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 1000;
`;

export const Blocker = styled.div`
    position: fixed;
    background: rgba(0, 0, 0, .7);
    width: 100%;
    height: 100%;
`;

export const ModalWindowBody = styled.div`
    width: 480px;
    height: 200px;
    background: #0c202e;
    border-radius: 4px;
    z-index: 1;
    display: flex;
    grid-template-rows: 1fr 6fr 1fr;         
    position: relative;
    align-items: center;
    flex-direction: column;
`;

export const Header = styled.div`
    margin-top: 5%;
    margin-bottom: 5%;
    padding: 18px 24px;
`;

export const Title = styled.div`
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 25px;
    color: #fff;
`;

export const CloseButton = styled.div`
    cursor: pointer;
    display: flex;
    justify-content: right;
    align-items: center;
`;

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 18px 24px;

    &::-webkit-scrollbar {
        width: 4px;
    }

    &::-webkit-scrollbar-track {
        border-radius: 100px;
    }

    &::-webkit-scrollbar-thumb {
      background: #263845;;
      border-radius: 10px;
    }
`;

export const Footer = styled.div`
    padding: 18px 24px;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

ModalWindow.Blocker = Blocker;

ModalWindowBody.Header = Header;

ModalWindowBody.Header.Title = Title;

ModalWindowBody.Container = Container;

ModalWindowBody.Footer = Footer;

