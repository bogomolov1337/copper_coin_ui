import React from 'react';

import SussesModalWindowApplyBtn from "./ApplyButton/SussesModalWindowApplyBtn";
import {ModalWindow, ModalWindowBody} from "./style";

const SuccessModalWindow = (props) => {
    return (
        <ModalWindow>
            <ModalWindow.Blocker/>
            <ModalWindowBody >
                <ModalWindowBody.Header>
                    <ModalWindowBody.Header.Title>Password recovery successful.</ModalWindowBody.Header.Title>
                </ModalWindowBody.Header>
                <ModalWindowBody.Footer>
                    <SussesModalWindowApplyBtn />
                </ModalWindowBody.Footer>
            </ModalWindowBody>
        </ModalWindow>
    );
};

export default SuccessModalWindow;
