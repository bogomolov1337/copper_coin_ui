import React, {useContext} from 'react';
import {UserContext} from '../../../userContext/UserContext';
import {GetSmsPinApplyButton} from '../../../login/Modal2FaWindow/ModalWindow/ApplyButton/style';
import {NavLink} from "react-router-dom";
import {useDispatch} from "react-redux";
import actions from "../../../../redux/actions";
import {setSuccessForgotWindowState} from "../../../../redux/actionsCreator";

const SussesModalWindowApplyBtn = (props) => {
    const {inputValue, payload, loginUser} = props;
    const disp = useDispatch();

    const {
        actions: {
            showMessageError,
        },
        selectors: {},
    } = useContext(UserContext);

    const handleClick = () => {
        showMessageError('')
        disp(setSuccessForgotWindowState(false))
    };

    return (
        <NavLink to={'/login'}>
        <GetSmsPinApplyButton id={'forgot-password.success_btn'} onClick={handleClick}>Ok</GetSmsPinApplyButton>
        </NavLink>
    );
};

export default SussesModalWindowApplyBtn;
