import React from 'react';
import {Wrapper, Label, ErrorMsg, InputWrapper} from './style';

const FormInput = (props) => {
    const {
        labelText,
        inputType,
        onChange,
        styleValidation,
        onKeyPressHandler,
        message, id
    } = props;

    return (
        <Wrapper data-at="form-input_wrapper">
        <Wrapper.Label data-at="wrapper_label">
                <Label.Text
                    data-at="label_text"
                    children={labelText}
                />
            </Wrapper.Label>
            <InputWrapper>
            <Wrapper.Input
                id={id}
                type={inputType}
                data-at="wrapper_input"
                onChange={onChange}
                borderColor={styleValidation}
                onKeyPress={onKeyPressHandler}
            />
            <ErrorMsg>{message}</ErrorMsg>
            </InputWrapper>
        </Wrapper>
    );
};

export default FormInput;
