import React, {useContext} from 'react';
import {useTranslation} from 'react-i18next';
import {
    Body,
    Button,
    ButtonContainer,
    ButtonReg,
    ButtonRegistrContainer,
    Company,
    ForgotPasswordLink,
    ItemsContainer,
    RegTitle,
    Text,
    Wrapper
} from './style';
import FormInput from './FormInput/FormInput';
import {UserContext} from '../userContext/UserContext';
import logo from "../../logo.png";
import Modal2FAWindow from "./Modal2FaWindow/ModalWindow/Modal2FAWindow";
import {NavLink} from "react-router-dom";

const Login = (props) => {
    const {
        onLoginChange,
        onPasswordChange,
        onAuthSubmit,
        userNameColor,
        passColor,
        onKeyPressHandler,
        passwordErrorMessage,
        loginErrorMessage,
        user,
    } = props;
    const {t} = useTranslation();
    const {selectors: {isOpenModal2faPinWindow, errorMessage}, actions: {loginUser, showMessageError}} = useContext(UserContext);

    const handleClick = () => {
        showMessageError('')
        onAuthSubmit()
    }

    const handleClickReg = () => {
        showMessageError('')
    }
    let payload = {loginOrEmail: user.login, pass: user.password, authCode: ''};

    return (

        <Wrapper data-at="wrapper">
            <ItemsContainer.Company>
                <Company.Logo src={logo}/>
                <Company.Name>Copper coin - don't waste your money</Company.Name>
                <RegTitle>Don't have account?</RegTitle>
                <ButtonRegistrContainer>
                    <NavLink to='/registration'>
                        <ButtonReg id={'login.link_to_registration'} onClick={handleClickReg}>
                            <Button.Label children={'Registration'}/>
                        </ButtonReg>
                    </NavLink>
                </ButtonRegistrContainer>
            </ItemsContainer.Company>
            {isOpenModal2faPinWindow && <Modal2FAWindow functio={loginUser} payload={payload}/>}

            <Wrapper.Body data-at="wrapper body">
                <Text>Log in to work with website.</Text>
                <FormInput
                    id={'login.login_input'}
                    onChange={onLoginChange}
                    labelText={t('login.login_input')}
                    styleValidation={userNameColor}
                    onKeyPressHandler={onKeyPressHandler}
                    message={loginErrorMessage}
                />

                <FormInput
                    id={'login.password_input'}
                    onChange={onPasswordChange}
                    labelText={t('login.password_input')}
                    inputType="password"
                    styleValidation={passColor}
                    onKeyPressHandler={onKeyPressHandler}
                    message={passwordErrorMessage}
                />

                <NavLink to='/forgot-password'>
                <ForgotPasswordLink id={'login.link_to_forgot_pass'}>Forgot your password?</ForgotPasswordLink>
                </NavLink>

                <Body.ButtonContainer>
                    <ButtonContainer.Button id={'login.login_button'} onClick={handleClick}>
                        <Button.Label children={t('login.login_button')}/>
                    </ButtonContainer.Button>
                </Body.ButtonContainer>
            </Wrapper.Body>
        </Wrapper>
    );
};

export default Login;
