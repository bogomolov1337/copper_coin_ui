import styled from 'styled-components';

const bodyWidth = 800;
const bodyHeight = 400;
const bodyBorderRadius = 20;
const inputBorderWidth = 1;
const buttonContainerWidth = 150;
const buttonWidth = 100;
const buttonHeight = 30;
const buttonBorderRadius = 10;
const buttonLabelHeight = 16;
const buttonLabelFontSize = 18;

export const Wrapper = styled.div`
    width: 100%;
    height: 100%;
    z-index: 1;
    display: flex;
  
    flex-direction: column;
    background-color: #0c202e;
`;

export const Body = styled.div`
    margin-left: auto; 
    margin-right: auto; 
    margin-top: auto; 
    margin-bottom: auto; 
    width: ${bodyWidth}px;
    min-height: ${bodyHeight}px;
    height: auto;
    background-color: #142836;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    border-radius: ${bodyBorderRadius}px;
  
`;

export const Button = styled.button`
    width: ${buttonWidth}px;
    height: ${buttonHeight}px;
    border-radius: ${buttonBorderRadius}px;
    border: ${inputBorderWidth}px solid #FF9900;
    background-color: #142836;
    margin-top: 5px;
    outline: 0 !important;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #FF9900;
    
    &:hover {
      background-color: #ff9900;
      transition: all .3s cubic-bezier(.47,0,.745,.715);
      color: #142836;
    }
`;

export const ButtonReg = styled.button`
    width: 150px;
    height: ${buttonHeight}px;
    border-radius: ${buttonBorderRadius}px;
    border: ${inputBorderWidth}px solid #FF9900;
    background-color: #142836;
    outline: 0 !important;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #FF9900;
    
    margin-top: 15%; 
  
    
    &:hover {
      background-color: #ff9900;
      transition: all .3s cubic-bezier(.47,0,.745,.715);
      color: #142836;
    }
`;
export const RegTitle = styled.h1`
    margin-top: 15px; 
    margin-left: 43%; 
    font-family: "Roboto";
    color: #fff;
    font-size: 14px;
    font-weight: normal;
   
    padding: 0;
    user-select: none;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

Button.Label = styled.label`
    height: ${buttonLabelHeight}px;
    display: flex;
    justify-content: center;
    font-size: ${buttonLabelFontSize}px;
    text-transform: uppercase;
    font-family: 'Roboto', sans-serif;
    cursor: pointer;
`;

export const ButtonContainer = styled.div`
    width: ${buttonContainerWidth}px;
    padding-bottom: 10px;
    padding-left: 17px;
    margin-left: auto; 
    margin-right: auto; 
    margin-top: auto; 
    margin-bottom: auto; 
`;

export const ButtonRegistrContainer = styled.div`
    width: ${buttonContainerWidth}px;
    padding-bottom: 10px;
    padding-left: 17px; 
`;

export const ErrorMsg = styled.div`
    position: absolute;
    bottom: 7px;
    color: #f70303e6;
    width: 100%;
    text-align: center;
    margin-left: auto; 
    margin-right: auto; 
    margin-top: auto; 
    margin-bottom: 20%; 
`;

export const ItemsContainer = styled.div`
    height: 100%;
    flex-direction: row;
    align-items: center;
    width: 100%;
`;

export const Company = styled.div`
    height: 10%;
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export const Logo = styled.img`
    width: 60px;
    height: 60px;
    margin-left: 30px;
    margin-top: 30px;
`;


export const Name = styled.h1`
    font-family: "Roboto";
    color: #fff;
    font-size: 22px;
    font-weight: normal;
    margin-top: 40px;
    margin-left: 20px;

    padding: 0;
    user-select: none;
`;

export const Text = styled.h1`
    margin-left: 5%; 
    margin-right: auto; 
    margin-top: 5%; 
    font-family: "Roboto";
    color: #fff;
    font-size: 14px;
    font-weight: normal;
   
    padding: 0;
    user-select: none;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    
`;

export const InputTitle = styled.h1`
    margin-left: 20%; 
    margin-right: auto; 
    margin-top: 5%; 
    font-family: "Roboto";
    color: #fff;
    font-size: 14px;
    font-weight: normal;
   
    padding: 0;
    user-select: none;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;


export const ForgotPasswordLink = styled.h1`
    font-family: "Roboto";
    color: #fff;
    font-size: 14px;
    font-weight: normal;
  
    padding: 0;
    user-select: none;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    cursor: pointer;
`;

ItemsContainer.Company = Company;
Company.Logo = Logo;
Company.Name = Name;

ButtonContainer.Button = Button;

Body.ButtonContainer = ButtonContainer;



Body.ErrorMsg = ErrorMsg;

Wrapper.Body = Body;
