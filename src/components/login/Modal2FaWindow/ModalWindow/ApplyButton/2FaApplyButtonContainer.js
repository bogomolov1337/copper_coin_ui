import React, {useContext} from 'react';
import {UserContext} from '../../../../userContext/UserContext';
import {GetSmsPinApplyButton} from './style';

const GetSmsPinApplyButtonContainer = (props) => {
    const {inputValue, payload, loginUser} = props;

    const {
        actions: {
            showMessageError,
        },
        selectors: {},
    } = useContext(UserContext);

    const handleClick = () => {
        if (inputValue.length !== 0) {
            payload.authCode = inputValue;
            loginUser(payload)
        } else {
            showMessageError('Field is Empty');
        }
    };

    return (
        <GetSmsPinApplyButton id={'auth-code-window.ok_btn'} onClick={handleClick}>Ok</GetSmsPinApplyButton>
    );
};

export default GetSmsPinApplyButtonContainer;
