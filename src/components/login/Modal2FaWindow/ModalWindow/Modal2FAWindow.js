import React, {useContext, useEffect, useState} from 'react';
import {UserContext} from '../../../userContext/UserContext';
import {ModalWindow, ModalWindowBody, Text2Fa,} from './style';
import {Formik} from 'formik';
import FormGroup from "../../../maine-page/PremiumModal/formGroup/FormGroup";
import * as yup from "yup";
import {OkButton} from "../../../maine-page/PremiumModal/Buttons/style";

const Modal2FAWindow = (props) => {
    const {functio, payload} = props;

    const [inputText, setInputText] = useState('');

    const {
        actions: {on2faModalChange},
        selectors: {
            errorMessage,
            messageColor,
            borderColor,
        },
    } = useContext(UserContext);

    useEffect(() => {
        const close = (e) => {
            if (e.keyCode === 27) {
                on2faModalChange();
            }
        };
        window.addEventListener('keydown', close);
        return () => window.removeEventListener('keydown', close);
    }, []);

    const handleChang = (e) => {
        setInputText(e.target.value.trim());
    };

const onSubmit = (values) => {
    payload.authCode = values.authCode;
    functio(payload)
}

    return (
        <ModalWindow>
            <ModalWindow.Blocker/>
            <ModalWindowBody>
                <ModalWindowBody.Header>
                    <ModalWindowBody.Header.Title>Confirmation code</ModalWindowBody.Header.Title>
                    <ModalWindowBody.Header.CloseButton onClick={on2faModalChange} title="Close">
                        <CloseIcon
                            width="14"
                            height="14"
                            viewBox="0 0 14 14"
                        />
                    </ModalWindowBody.Header.CloseButton>
                </ModalWindowBody.Header>
                <ModalWindowBody.Container>
                    <Text2Fa>Enter the code from Google authentication</Text2Fa>
                    <Formik
                        initialValues={{
                            authCode: '',
                        }}
                        validateOnBlur
                        onSubmit={(values) => onSubmit(values)}
                        validationSchema={yup.object().shape({
                            authCode: yup.string().typeError('validation.required_field')
                                    .matches(/(?=.*[0-9]{6})/, 'Only 6 digits')
                                    .required('Field is required')})}
                    >
                        {({
                              values, errors, touched, handleChange, handleBlur, isValid, handleSubmit, dirty,
                          }) => (
                            <>
                                <FormGroup
                                    errorData={touched.authCode && errors.authCode }
                                    type="text"
                                    name="authCode"
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    value={values.login}
                                    maxLength={6}
                                />
                                <ModalWindowBody.Footer>

                                    <OkButton disabled={!isValid && !dirty}
                                            onClick={handleSubmit}
                                            type="submit"
                                            children={"Ok"}
                                    />
                                </ModalWindowBody.Footer>
                            </>)}
                    < /Formik>
                    <ModalWindowBody.Error messageColor={"red"}>{errorMessage ? "You confused something. Try again" : ""}</ModalWindowBody.Error>

                </ModalWindowBody.Container>
            </ModalWindowBody>
        </ModalWindow>
    );
};

export default Modal2FAWindow;

export function CloseIcon(props) {
    return (
        <svg {...props}>
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M7.00008 8.41429L12.293 13.7072L13.7072 12.293L8.41429 7.00008L13.7072 1.70718L12.293 0.292969L7.00008
         5.58586L1.70718 0.292969L0.292969 1.70718L5.58586 7.00008L0.292969 12.293L1.70718 13.7072L7.00008 8.41429Z"
                fill="#79838A"
            />
        </svg>
    );
}