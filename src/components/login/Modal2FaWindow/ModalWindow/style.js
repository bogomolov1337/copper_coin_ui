import styled from 'styled-components';

export const ModalWindow = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 4;
`;

export const Blocker = styled.div`
    position: fixed;
    background: rgba(0, 0, 0, .7);
    width: 100%;
    height: 100%;
`;

export const ModalWindowBody = styled.div`
    width: 480px;
    height: 350px;
    background: #0c202e;
    border-radius: 4px;
    z-index: 1;
    display: grid;
    grid-template-rows: 1fr 6fr 1fr;         
    position: relative;
`;

export const Text2Fa = styled.div`
    margin-left: 20%; 
    margin-right: auto; 
    margin-bottom: 5%; 
    font-family: "Roboto";
    color: #fff;
    font-size: 14px;
    font-weight: normal;
   
    padding: 0;
    user-select: none;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;



export const Header = styled.div`
    width: 100%;
    padding: 18px 24px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

export const Title = styled.div`
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 25px;
    color: #fff;
`;

export const CloseButton = styled.div`
    cursor: pointer;
    display: flex;
    justify-content: right;
    align-items: center;
`;

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 18px 24px;

    &::-webkit-scrollbar {
        width: 4px;
    }

    &::-webkit-scrollbar-track {
        border-radius: 100px;
    }

    &::-webkit-scrollbar-thumb {
      background: #263845;;
      border-radius: 10px;
    }
`;

export const Footer = styled.div`
    padding: 18px 24px;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

export const PinInput = styled.input`
    width: 40%;
    height: 48px;
    padding: 0 12px;
    outline: none;
    border-radius: 4px;
    font-size: 20px;
    border: 1px;
    color: #D3DBE0;
    background-color: #0c202e;
    font-family: 'Roboto', sans-serif;
    border: 1px solid ${(props) => props.borderColor};
      
    :-webkit-autofill,
    :-webkit-autofill:hover, 
    :-webkit-autofill:focus {
        -webkit-text-fill-color: #0c202e;
        -webkit-box-shadow: 0 0 0px 1000px #FDF5E6 inset;
        transition: background-color 5000s ease-in-out 0s;
    }
    
    &::-ms-clear, &::-ms-reveal {
      display: none;
    }
`;

export const Error = styled.div`
    height: 10px;
    margin-top: 5%;
    color: ${(props) => props.messageColor};
    width: 100%;
    text-align: center;
`;

ModalWindow.Blocker = Blocker;

ModalWindowBody.Header = Header;

ModalWindowBody.Header.Title = Title;

ModalWindowBody.Header.CloseButton = CloseButton;

ModalWindowBody.Container = Container;

ModalWindowBody.Container.PinInput = PinInput;

ModalWindowBody.Footer = Footer;

ModalWindowBody.Error = Error;
