import styled from 'styled-components';

export const SmsButtonWrapper = styled.div`
   & > button {
    color: ${(props) => props.buttonColor};
        border-color: ${(props) => props.buttonBorderColor};
        background-color: ${(props) => props.buttonBackgroundColor};
        font-family: 'Roboto', sans-serif;
        height: 2.6em;
        margin: 0.8em 10px 0 0.2em;
        outline: none !important;

        &:hover{
            background-color: ${(props) => props.buttonHoverBackgroundColor};
        }

        &:active{
            outline: none;
            border: none;
        }
   }
`;
