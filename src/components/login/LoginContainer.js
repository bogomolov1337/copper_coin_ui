import React, {useContext, useState} from 'react';
import {useDispatch} from 'react-redux';
import * as actions from '../../redux/actionsCreator';
import Login from './Login';
import {UserContext} from "../userContext/UserContext";

const LoginContainer = () => {
    const [userName, setUserName] = useState('');
    const [pass, setPass] = useState('');
    const [userNameColor, setUserNameColor] = useState('none');
    const [passColor, setPassColor] = useState('none');
    const dispatch = useDispatch();
    const {actions: {on2faModalChange}} = useContext(UserContext);
    const [passwordErrorMessage, setPasswordErrorMessage] = useState('');
    const [loginErrorMessage, setLoginErrorMessage] = useState('');


    const onLoginChange = (e) => {
        setUserName(e.target.value);
        setUserNameColor(e.target.value.trim().length ? 'none' : 'red');
        setLoginErrorMessage(e.target.value.trim().length ? '' : 'Empty email/login field.');
    };

    const onPasswordChange = (e) => {
        setPass(e.target.value);
        setPassColor(e.target.value.trim().length ? 'none' : 'red');
        setPasswordErrorMessage(e.target.value.trim().length ? '' : 'Empty password field.');

    };

    const onAuthSubmit = () => {
        if (userName.trim().length && pass.trim().length) {
            setPasswordErrorMessage('');
            setLoginErrorMessage('')
            on2faModalChange()
            return;
        }

        if (!userName.trim().length) {
            setPassColor(pass.trim().length ? 'none' : 'red');
            setLoginErrorMessage('Empty email/login field.')
        } else {
            setLoginErrorMessage('')
        }

        if (!pass.trim().length) {
            setUserNameColor(userName.trim().length ? 'none' : 'red');
            setPasswordErrorMessage('Empty password field.');
        } else {
            setPasswordErrorMessage('');
        }
};

const onKeyPressHandler = (event) => {
    if (event.key === 'Enter') {
        onAuthSubmit();
    }
};

return (
    <Login
        user={{login: userName, password: pass}}
        onLoginChange={onLoginChange}
        onPasswordChange={onPasswordChange}
        onAuthSubmit={onAuthSubmit}
        userNameColor={userNameColor}
        passColor={passColor}
        passwordErrorMessage={passwordErrorMessage}
        loginErrorMessage={loginErrorMessage}
        onKeyPressHandler={onKeyPressHandler}
    />
);
};

export default LoginContainer;
