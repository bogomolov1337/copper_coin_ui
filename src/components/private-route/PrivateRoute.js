import React from "react";
import {Route, Router, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import asyncComponent from '../../helpers/AsyncFunc';
import {
    CopperCoin,
} from "./style";

const PrivateRoute = ({history, isAuthenticated}) => {
    return (
        <Router history={history}>
            {isAuthenticated !== true && <Redirect to="/login"/>}
           <CopperCoin>
                <Route
                    exact
                    path={'/login'}>
                    { isAuthenticated && <Redirect to="/"/>}
                </Route>
                <Route
                    exact
                    path={'/login'}
                    component={asyncComponent(() => import('../login/LoginContainer'))}
                />
               <Route
                    exact
                    path={'/registration'}
                    component={asyncComponent(() => import('../registration/Registration'))}
                />

               <Route
                    exact
                    path={'/forgot-password'}
                    component={asyncComponent(() => import('../forgot-password/ForgotPasswordContainer'))}
                />

                <Route
                    exact
                    path={'/'}
                    component={asyncComponent(() => import('../Dashboard'))}
                />
                <Route
                    exact
                    path={'/dashboard'}
                    component={asyncComponent(() => import('../Dashboard'))}
                />
            </CopperCoin>
        </Router>
    );
}

export default connect(state => ({
    isAuthenticated: state.rootReducer.auth.isAuth
}))(PrivateRoute);
