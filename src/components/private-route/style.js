import styled from 'styled-components';

export const  CopperCoin = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 1920px;
    width: 100%;
    height: 100vh;
    margin: 0 auto;
    padding: 0;
    overflow: hidden;
`;
