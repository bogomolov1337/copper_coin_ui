import styled from 'styled-components';

export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    min-height: 100%;
`;

export const Content = styled.div`
    display: flex;
    height: 100vh;
    flex-direction: column;
    justify-content: space-between;
`;

export const Body = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    flex-direction: row;
    justify-content: flex-start;
`;

export const Footer = styled.div`
    display: flex;
    height: 20%;
    width: 100%;
    flex: 0 0 auto;
    margin-bottom: 10px;
`;
