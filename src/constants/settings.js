export const defaultPageRequest = { count: 1000, pageNumber: 1, pageSize: 100 };
export const datePickerPattern = 'dd/MM/yyyy';
export const refreshtokeninitialdelay = 60000;
export const refreshtokeninterval = 180000;
export const locale = 'ru';

export function loadDefaultStorages() {
  return [
    { value: 'cold', label: 'COLD' },
    { value: 'hot', label: 'HOT' },
  ];
}

export const customStyles = {
  input: (provided) => (
    {
      ...provided, color: '#bdbdbd', fontSize: '17px', fontWeight: 'bold',
    }),
  menuList: (provided) => ({
    ...provided,
    backgroundColor: '#0c202e',
    '::-webkit-scrollbar':
        { width: '5px' },
    '::-webkit-scrollbar-track':
          { background: '#0c202e' },
    '::-webkit-scrollbar-thumb':
        { background: '#244152' },
    '::-webkit-scrollbar-thumb:hover':
        { background: '#555' },
  }),
  option: (provided, { isSelected }) => ({
    ...provided,
    backgroundColor: isSelected ? '#1E3644' : '#0c202e',
    color: 'white',
    overflow: 'hidden',
    fontWeight: 'bold',
    ':hover':
          { background: '#244152' },
    cursor: 'pointer',
    fontFamily: '\'Roboto\', sans-serif',
  }),
  control: (provided) => ({
    ...provided,
    fontWeight: 'bold',
    overflow: 'hidden',
    background: '#263845',
    cursor: 'pointer',
    borderColor: '#263845',
  }),
  singleValue: (provided) => ({
    ...provided,
    color: 'white',
    overflow: 'hidden',
    fontWeight: 'bold',
  }),
};

export const modalWindowCustomStyles = {
  multiValueLabel: (provided) => (
    { ...provided, color: '#ececec' }
  ),
  multiValue: (provided) => (
    { ...provided, background: '#8c8989', maxWidth: '95%' }
  ),
  valueContainer: (provided) => ({
    ...provided,
    maxHeight: '68px',
    overflowY: 'scroll',
    '::-webkit-scrollbar': { background: '#263845', width: '4px' },
    '::-webkit-scrollbar-thumb': { background: '#061E2F' },
  }),
  ...customStyles,
};

export const iconsColors = { color: '#FF9900' };

export const iconsDirectionColors = { color: '#79838A' };

export const saveToFileOptions = {
  filename: 'txsbackup',
  fieldSeparator: ',',
  quoteStrings: '"',
  decimalSeparator: '.',
  showLabels: true,
  showTitle: true,
  title: 'transactions backup',
  useTextFile: false,
  useBom: true,
  useKeysAsHeaders: true,
};
