export const errorInvalidParameters = 'Invalid login or password.';
export const errorServer = 'Something went wrong. Please, try again later.';
export const invalidToken = 'Invalid token specified.';
