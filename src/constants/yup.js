import * as yup from 'yup';

export const confirmRegistrationSchema = yup.object().shape({
    login: yup.string().typeError('validation.required_field').required('validation.required_field'),
    newPassword: yup.string().typeError('validation.required_field')
        .matches(/(?=.*[0-9])/, 'validation.minimum_one_number')
        .matches(/(?=.*[a-z])/, 'validation.minimum_one_letter')
        .matches(/(?=.*[A-Z])/, 'validation.minimum_one_letter_uppercase')
        .min(8, 'validation.minimum_characters')
        .required('validation.required_field'),
    confirmPassword: yup.string().oneOf([yup.ref('newPassword')], 'validation.confirm_password_not_match').required('validation.required_field'),
    passFromSMS: yup.string().required('validation.required_field'),
    passFromSA: yup.string().required('validation.required_field'),
});
