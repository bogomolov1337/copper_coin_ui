const urlUtils = {
  BACK_END_URL: process.env.REACT_APP_BACK_END_URL,
};

export default urlUtils;
