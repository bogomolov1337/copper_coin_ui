import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createBrowserHistory as createHistory } from 'history';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './redux/reducers';
import rootSaga from './redux/sagas/rootSaga';

const sagaMiddleware = createSagaMiddleware();
const history = createHistory();
const routeMiddleware = routerMiddleware(history);
const middlewares = [thunk, sagaMiddleware, routeMiddleware];

const saveState = (state) => {
  try {
    const serialisedState = JSON.stringify(state);

    window.sessionStorage.setItem('app_state', serialisedState);
  } catch (err) {
    console.error(err);
  }
};

const loadState = () => {
  try {
    const serialisedState = window.sessionStorage.getItem('app_state');

    if (!serialisedState) return undefined;

    return JSON.parse(serialisedState);
  } catch (err) {
    return undefined;
  }
};

const oldState = loadState();

const store = createStore(
  combineReducers({
    rootReducer,
    router: routerReducer,
  }), oldState,
  composeWithDevTools(applyMiddleware(...middlewares)),
);

store.subscribe(() => {
  saveState(store.getState());
});

sagaMiddleware.run(rootSaga);
window.store = store;

export { store, history };
