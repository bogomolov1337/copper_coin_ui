import EN from './en/en.json';
import RU from './ru/ru.json';

export const resources = {
  en: { translation: EN },
  ru: { translation: RU },
};
